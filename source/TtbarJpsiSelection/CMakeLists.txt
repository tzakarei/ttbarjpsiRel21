################################################################################
# Package: TtbarJpsiSelection
################################################################################

# Declare the package name:
atlas_subdir( TtbarJpsiSelection None )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PUBLIC
                          TopAnalysis
                          TopObjectSelectionTools
                          TopEventSelectionTools
                          TopEvent
                          TopConfiguration
                          xAODBPhys
                          xAODMuon
			  AsgTools
                        )

# This package uses ROOT:
find_package( ROOT REQUIRED COMPONENTS Core Gpad Tree Hist RIO MathCore Graf )

# Custom definitions needed for this package:
add_definitions( -g )

# Generate a CINT dictionary source file:
atlas_add_root_dictionary( TtbarJpsiSelectionLib _cintDictSource
                           ROOT_HEADERS Root/LinkDef.h
                           EXTERNAL_PACKAGES ROOT )

# Component(s) in the package:
atlas_add_library( TtbarJpsiSelectionLib
                   Root/*.cxx Root/*.h 
                   TtbarJpsiSelection/*.h TtbarJpsiSelection/*.cxx
                   TtbarJpsiSelection/*/*.h TtbarJpsiSelection/*/*.cxx
                   ${_cintDictSource}
                   PUBLIC_HEADERS TtbarJpsiSelection
                   LINK_LIBRARIES TopAnalysis
                                  TopObjectSelectionTools
                                  TopEventSelectionTools
                                  TopEvent
                                  TopConfiguration
                                  xAODBPhysLib
                                  xAODMuon
				  
                                  ${ROOT_LIBRARIES}
			    	   xAODEventInfo
                                    xAODRootAccess xAODBase xAODCore xAODEgamma xAODTracking 
                                    xAODMuon xAODBPhysLib
                                    xAODJet
                                    TriggerMatchingToolLib
                                    InDetTrackSelectionToolLib TrigDecisionToolLib TrigConfInterfaces
                                    AsgTools
                                    MuonMomentumCorrectionsLib MuonSelectorToolsLib MuonAnalysisInterfacesLib
                                    EgammaAnalysisInterfacesLib ElectronPhotonSelectorToolsLib
                                    xAODParticleEvent 
                   INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
                 )

# Install files from the package:
atlas_install_data( share/* )

