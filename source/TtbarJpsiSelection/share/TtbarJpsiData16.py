import TopExamples.grid
import TopExamples.ami

#########################################################################
# Derivations for reprocessed 2016 full dataset - ICHEP2016 samples (p2667)
# for AT>=2.4.12
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TopDerivationMC15cList#Already_available_derivations_fo
# The runs listed below (30 runs) are those that are included in this NEW GRL for the 2016 data (DS1 dataset): data16_13TeV.periodAllYear_DetStatus-v79-pro20-06_DQDefects-00-02-02_PHYS_StandardGRL_All_Good_25ns.xml This GRL contains periods A3-B3 and corresponds to an integrated luminosity of 5118.35 pb-1, calculated with the tag OflLumi-13TeV-001.
##########################################################################


TopExamples.grid.Add('Data16-retry').datasets = [
]

TopExamples.grid.Add('Data16-periodA3').datasets = [
    'data16_13TeV.00297730.physics_Main.merge.DAOD_TOPQ1.f694_m1583_p2667',
]

TopExamples.grid.Add('Data16-periodA5').datasets = [
    'data16_13TeV.00298595.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
    'data16_13TeV.00298609.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
    'data16_13TeV.00298633.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
    'data16_13TeV.00298687.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
    'data16_13TeV.00298690.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
    'data16_13TeV.00298771.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
    'data16_13TeV.00298773.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
    'data16_13TeV.00298862.physics_Main.merge.DAOD_TOPQ1.f696_m1588_p2667',
    'data16_13TeV.00298967.physics_Main.merge.DAOD_TOPQ1.f696_m1588_p2667',
]

TopExamples.grid.Add('Data16-periodA6').datasets = [
    'data16_13TeV.00299055.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
]

TopExamples.grid.Add('Data16-periodA7').datasets = [
    'data16_13TeV.00299144.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
    'data16_13TeV.00299147.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
]

TopExamples.grid.Add('Data16-periodA8').datasets = [
    'data16_13TeV.00299184.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
    'data16_13TeV.00299243.physics_Main.merge.DAOD_TOPQ1.f698_m1594_p2667',
]

TopExamples.grid.Add('Data16-periodA10').datasets = [
    'data16_13TeV.00299584.physics_Main.merge.DAOD_TOPQ1.f703_m1600_p2667',
    'data16_13TeV.00300279.physics_Main.merge.DAOD_TOPQ1.f705_m1606_p2667',
]

TopExamples.grid.Add('Data16-periodB1').datasets = [
    'data16_13TeV.00300345.physics_Main.merge.DAOD_TOPQ1.f705_m1606_p2667',
    'data16_13TeV.00300415.physics_Main.merge.DAOD_TOPQ1.f705_m1606_p2667',
]

TopExamples.grid.Add('Data16-periodB2').datasets = [
    'data16_13TeV.00300418.physics_Main.merge.DAOD_TOPQ1.f705_m1606_p2667',
    'data16_13TeV.00300487.physics_Main.merge.DAOD_TOPQ1.f705_m1606_p2667',
    'data16_13TeV.00300540.physics_Main.merge.DAOD_TOPQ1.f705_m1606_p2667',
    'data16_13TeV.00300571.physics_Main.merge.DAOD_TOPQ1.f705_m1606_p2667',
    'data16_13TeV.00300600.physics_Main.merge.DAOD_TOPQ1.f708_m1606_p2667',
    'data16_13TeV.00300655.physics_Main.merge.DAOD_TOPQ1.f708_m1606_p2667',
    'data16_13TeV.00300687.physics_Main.merge.DAOD_TOPQ1.f708_m1606_p2667',
]

TopExamples.grid.Add('Data16-periodB3').datasets = [
    'data16_13TeV.00300784.physics_Main.merge.DAOD_TOPQ1.f708_m1606_p2667',
    'data16_13TeV.00300800.physics_Main.merge.DAOD_TOPQ1.f708_m1606_p2667',
    'data16_13TeV.00300863.physics_Main.merge.DAOD_TOPQ1.f708_m1606_p2667',
    'data16_13TeV.00300908.physics_Main.merge.DAOD_TOPQ1.f708_m1606_p2667',
]

TopExamples.grid.Add('Data16-periodC1').datasets = [
    'data16_13TeV.00301912.physics_Main.merge.DAOD_TOPQ1.f709_m1611_p2667',
    'data16_13TeV.00301918.physics_Main.merge.DAOD_TOPQ1.f709_m1611_p2667',
]

TopExamples.grid.Add('Data16-periodC2').datasets = [
    'data16_13TeV.00301932.physics_Main.merge.DAOD_TOPQ1.f709_m1611_p2667',
    'data16_13TeV.00301973.physics_Main.merge.DAOD_TOPQ1.f709_m1611_p2667',
]

TopExamples.grid.Add('Data16-periodC3').datasets = [
    'data16_13TeV.00302053.physics_Main.merge.DAOD_TOPQ1.f709_m1611_p2689',
    'data16_13TeV.00302137.physics_Main.merge.DAOD_TOPQ1.f709_m1620_p2689',
    'data16_13TeV.00302265.physics_Main.merge.DAOD_TOPQ1.f709_m1620_p2689',
    'data16_13TeV.00302269.physics_Main.merge.DAOD_TOPQ1.f709_m1620_p2689',
    'data16_13TeV.00302300.physics_Main.merge.DAOD_TOPQ1.f711_m1620_p2689',
    'data16_13TeV.00302347.physics_Main.merge.DAOD_TOPQ1.f711_m1620_p2689',
    'data16_13TeV.00302380.physics_Main.merge.DAOD_TOPQ1.f711_m1620_p2689',
    'data16_13TeV.00302391.physics_Main.merge.DAOD_TOPQ1.f711_m1620_p2689',
]

TopExamples.grid.Add('Data16-periodC4').datasets = [
    'data16_13TeV.00302393.physics_Main.merge.DAOD_TOPQ1.f711_m1620_p2689',
]

TopExamples.grid.Add('Data16-periodD1').datasets = [
    'data16_13TeV.00302737.physics_Main.merge.DAOD_TOPQ1.f711_m1620_p2689',
]

TopExamples.grid.Add('Data16-periodD2').datasets = [
    'data16_13TeV.00302831.physics_Main.merge.DAOD_TOPQ1.f711_m1620_p2689',
]

TopExamples.grid.Add('Data16-periodD3').datasets = [
    'data16_13TeV.00302872.physics_Main.merge.DAOD_TOPQ1.f716_m1620_p2689',
]

TopExamples.grid.Add('Data16-periodD4').datasets = [
    'data16_13TeV.00302919.physics_Main.merge.DAOD_TOPQ1.f715_m1620_p2689',
    'data16_13TeV.00302925.physics_Main.merge.DAOD_TOPQ1.f715_m1620_p2689',
    'data16_13TeV.00302956.physics_Main.merge.DAOD_TOPQ1.f715_m1620_p2689',
]

TopExamples.grid.Add('Data16-periodD5').datasets = [
    'data16_13TeV.00303007.physics_Main.merge.DAOD_TOPQ1.f715_m1620_p2689',
]

TopExamples.grid.Add('Data16-periodD7').datasets = [
    'data16_13TeV.00303079.physics_Main.merge.DAOD_TOPQ1.f715_m1620_p2689',
    'data16_13TeV.00303201.physics_Main.merge.DAOD_TOPQ1.f715_m1620_p2689',
    'data16_13TeV.00303208.physics_Main.merge.DAOD_TOPQ1.f715_m1620_p2689',
    'data16_13TeV.00303264.physics_Main.merge.DAOD_TOPQ1.f715_m1620_p2689',
    'data16_13TeV.00303266.physics_Main.merge.DAOD_TOPQ1.f715_m1620_p2689',    
    'data16_13TeV.00303291.physics_Main.merge.DAOD_TOPQ1.f716_m1620_p2689',
    'data16_13TeV.00303304.physics_Main.merge.DAOD_TOPQ1.f716_m1620_p2689',
]

TopExamples.grid.Add('Data16-periodD8').datasets = [
    'data16_13TeV.00303338.physics_Main.merge.DAOD_TOPQ1.f716_m1620_p2689',
    'data16_13TeV.00303421.physics_Main.merge.DAOD_TOPQ1.f716_m1620_p2689',
    'data16_13TeV.00303499.physics_Main.merge.DAOD_TOPQ1.f716_m1620_p2689',
    'data16_13TeV.00303560.physics_Main.merge.DAOD_TOPQ1.f716_m1620_p2689', 
]
