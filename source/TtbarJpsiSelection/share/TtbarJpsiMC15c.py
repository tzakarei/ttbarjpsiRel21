import TopExamples.grid
############################################################
# Available mc15c derivations - ICHEP2016 samples (p2669)
############################################################

TopExamples.grid.Add('MyMC15c-retry').datasets = [
]

###############################
### ttbar (FS)
###############################
TopExamples.grid.Add("MyMC15c_ttbar_FS_nonallhad").datasets = [
    'mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.DAOD_TOPQ1.e3698_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410159.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttbar_nonallhad.merge.DAOD_TOPQ1.e4683_s2726_r7725_r7676_p2691',
    'mc15_13TeV.410160.aMcAtNloPythia8EvtGen_A14_NNPDF23_NNPDF30ME_ttbar_allhad.merge.DAOD_TOPQ1.e4683_s2726_r7725_r7676_p2691', 
    'mc15_13TeV.410500.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_nonallhad.merge.DAOD_TOPQ1.e4797_s2726_r7725_r7676_p2691', 
]

###############################
### ttbar (FS)
###############################
TopExamples.grid.Add("MyMC15c_ttbar_AF_nonallhad").datasets = [
    'mc15_13TeV.410006.PowhegPythia8EvtGen_A14_ttbar_hdamp172p5_nonallhad.merge.DAOD_TOPQ1.e3836_a766_a810_r6282_p2501',
]

###############################
### ttbar dilepton (FS)
###############################
TopExamples.grid.Add("MyMC15c_ttbar_FS_dil").datasets = [
    'mc15_13TeV.410009.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_dil.merge.DAOD_TOPQ1.e4511_s2608_s2183_r7725_r7676_p2669',   
]

###############################
### ttbar mass points (FS)
###############################
TopExamples.grid.Add("MyMC15c_ttbar_FS_mass").datasets = [
    'mc15_13TeV.410037.PowhegPythiaEvtGen_P2012_ttbar_hdamp170_nonallhad.merge.DAOD_TOPQ1.e4529_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410038.PowhegPythiaEvtGen_P2012_ttbar_hdamp171p5_nonallhad.merge.DAOD_TOPQ1.e4529_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410039.PowhegPythiaEvtGen_P2012_ttbar_hdamp173p5_nonallhad.merge.DAOD_TOPQ1.e4529_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410040.PowhegPythiaEvtGen_P2012_ttbar_hdamp175_nonallhad.merge.DAOD_TOPQ1.e4529_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410041.PowhegPythiaEvtGen_P2012_ttbar_hdamp177p5_nonallhad.merge.DAOD_TOPQ1.e4529_s2608_s2183_r7725_r7676_p2669',
] 

###############################
### single top
###############################
TopExamples.grid.Add("MyMC15c_SingleTop_FS").datasets = [
    'mc15_13TeV.410011.PowhegPythiaEvtGen_P2012_singletop_tchan_lept_top.merge.DAOD_TOPQ1.e3824_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410012.PowhegPythiaEvtGen_P2012_singletop_tchan_lept_antitop.merge.DAOD_TOPQ1.e3824_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410013.PowhegPythiaEvtGen_P2012_Wt_inclusive_top.merge.DAOD_TOPQ1.e3753_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410014.PowhegPythiaEvtGen_P2012_Wt_inclusive_antitop.merge.DAOD_TOPQ1.e3753_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410015.PowhegPythiaEvtGen_P2012_Wt_dilepton_top.merge.DAOD_TOPQ1.e3753_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410016.PowhegPythiaEvtGen_P2012_Wt_dilepton_antitop.merge.DAOD_TOPQ1.e3753_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410025.PowhegPythiaEvtGen_P2012_SingleTopSchan_noAllHad_top.merge.DAOD_TOPQ1.e3998_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410026.PowhegPythiaEvtGen_P2012_SingleTopSchan_noAllHad_antitop.merge.DAOD_TOPQ1.e3998_s2608_s2183_r7725_r7676_p2669',
]

###############################
### ttV (FS)
###############################
TopExamples.grid.Add("MyMC15c_ttV").datasets = [
    'mc15_13TeV.410066.MadGraphPythia8EvtGen_A14NNPDF23LO_ttW_Np0.merge.DAOD_TOPQ1.e4111_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410067.MadGraphPythia8EvtGen_A14NNPDF23LO_ttW_Np1.merge.DAOD_TOPQ1.e4111_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410068.MadGraphPythia8EvtGen_A14NNPDF23LO_ttW_Np2.merge.DAOD_TOPQ1.e4111_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410069.MadGraphPythia8EvtGen_A14NNPDF23LO_ttZllonshell_Np0.merge.DAOD_TOPQ1.e4111_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410070.MadGraphPythia8EvtGen_A14NNPDF23LO_ttZllonshell_Np1.merge.DAOD_TOPQ1.e4111_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.410073.MadGraphPythia8EvtGen_A14NNPDF23LO_ttZnnqq_Np0.merge.DAOD_TOPQ1.e4631_s2726_r7725_r7676_p2669',
    'mc15_13TeV.410074.MadGraphPythia8EvtGen_A14NNPDF23LO_ttZnnqq_Np1.merge.DAOD_TOPQ1.e4631_s2726_r7725_r7676_p2669',
    'mc15_13TeV.410075.MadGraphPythia8EvtGen_A14NNPDF23LO_ttZnnqq_Np2.merge.DAOD_TOPQ1.e4631_s2726_r7725_r7676_p2669',
    'mc15_13TeV.410111.MadGraphPythia8EvtGen_A14NNPDF23LO_ttee_Np0.merge.DAOD_TOPQ1.e4632_s2726_r7725_r7676_p2669',
    'mc15_13TeV.410112.MadGraphPythia8EvtGen_A14NNPDF23LO_ttee_Np1.merge.DAOD_TOPQ1.e4632_s2726_r7725_r7676_p2669',
    'mc15_13TeV.410113.MadGraphPythia8EvtGen_A14NNPDF23LO_ttmumu_Np0.merge.DAOD_TOPQ1.e4632_s2726_r7725_r7676_p2669',
    'mc15_13TeV.410114.MadGraphPythia8EvtGen_A14NNPDF23LO_ttmumu_Np1.merge.DAOD_TOPQ1.e4632_s2726_r7725_r7676_p2669',
    'mc15_13TeV.410115.MadGraphPythia8EvtGen_A14NNPDF23LO_tttautau_Np0.merge.DAOD_TOPQ1.e4632_s2726_r7725_r7676_p2669',
    'mc15_13TeV.410116.MadGraphPythia8EvtGen_A14NNPDF23LO_tttautau_Np1.merge.DAOD_TOPQ1.e4632_s2726_r7725_r7676_p2669',
    'mc15_13TeV.410050.MadGraphPythiaEvtGen_P2012_tZ_4fl_tchan_noAllHad.merge.DAOD_TOPQ1.e4279_s2608_s2183_r7326_r6282_p2669',
]

###############################
### Diboson Sherpa
###############################

TopExamples.grid.Add("MyMC15c_Diboson_Sherpa").datasets = [
    'mc15_13TeV.361063.Sherpa_CT10_llll.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7725_r7676_p2669', 
    'mc15_13TeV.361064.Sherpa_CT10_lllvSFMinus.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361065.Sherpa_CT10_lllvOFMinus.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361066.Sherpa_CT10_lllvSFPlus.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361067.Sherpa_CT10_lllvOFPlus.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361068.Sherpa_CT10_llvv.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361069.Sherpa_CT10_llvvjj_ss_EW4.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361070.Sherpa_CT10_llvvjj_ss_EW6.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361071.Sherpa_CT10_lllvjj_EW6.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361072.Sherpa_CT10_lllljj_EW6.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361073.Sherpa_CT10_ggllll.merge.DAOD_TOPQ1.e3836_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361074.Sherpa_CT10_ggllll_M4l100.merge.DAOD_TOPQ1.e4047_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361075.Sherpa_CT10_ggllllNoHiggs.merge.DAOD_TOPQ1.e3911_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361076.Sherpa_CT10_ggllllOnlyHiggs.merge.DAOD_TOPQ1.e3911_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361077.Sherpa_CT10_ggllvv.merge.DAOD_TOPQ1.e4641_s2726_r7772_r7676_p2669',
    'mc15_13TeV.361078.Sherpa_CT10_ggllvvNoHiggs.merge.DAOD_TOPQ1.e4641_s2726_r7772_r7676_p2669',
    'mc15_13TeV.361079.Sherpa_CT10_ggllvvOnlyHiggs.merge.DAOD_TOPQ1.e3911_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361080.Sherpa_CT10_ggllvv_ZZcuts.merge.DAOD_TOPQ1.e3911_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361088.Sherpa_CT10_lvvv.merge.DAOD_TOPQ1.e4483_s2726_r7772_r7676_p2669',
    'mc15_13TeV.361089.Sherpa_CT10_vvvv.merge.DAOD_TOPQ1.e4483_s2726_r7772_r7676_p2669',
    'mc15_13TeV.361090.Sherpa_CT10_llll_M4l100.merge.DAOD_TOPQ1.e4555_s2726_r7772_r7676_p2669',
    'mc15_13TeV.361091.Sherpa_CT10_WplvWmqq_SHv21_improved.merge.DAOD_TOPQ1.e4607_s2726_r7725_r7676_p2669',
    'mc15_13TeV.361092.Sherpa_CT10_WpqqWmlv_SHv21_improved.merge.DAOD_TOPQ1.e4607_s2726_r7725_r7676_p2669',
    'mc15_13TeV.361093.Sherpa_CT10_WlvZqq_SHv21_improved.merge.DAOD_TOPQ1.e4607_s2726_r7725_r7676_p2669',
    'mc15_13TeV.361094.Sherpa_CT10_WqqZll_SHv21_improved.merge.DAOD_TOPQ1.e4607_s2726_r7725_r7676_p2669',
    'mc15_13TeV.361095.Sherpa_CT10_WqqZvv_SHv21_improved.merge.DAOD_TOPQ1.e4607_s2726_r7772_r7676_p2669',
    'mc15_13TeV.361096.Sherpa_CT10_ZqqZll_SHv21_improved.merge.DAOD_TOPQ1.e4607_s2726_r7725_r7676_p2669',
    'mc15_13TeV.361097.Sherpa_CT10_ZqqZvv_SHv21_improved.merge.DAOD_TOPQ1.e4607_s2726_r7772_r7676_p2669', 
]

###############################
### diboson PowhegPythia8
###############################
TopExamples.grid.Add('MyMC15c_Diboson_PowPy8').datasets = [
    'mc15_13TeV.361600.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvlv.merge.DAOD_TOPQ1.e4054_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361601.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvll_mll4.merge.DAOD_TOPQ1.e4475_s2726_r7725_r7676_p2669',
    'mc15_13TeV.361602.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvvv_mll4.merge.DAOD_TOPQ1.e4054_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361603.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZllll_mll4.merge.DAOD_TOPQ1.e4475_s2726_r7725_r7676_p2669',
    'mc15_13TeV.361604.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvll_mll4.merge.DAOD_TOPQ1.e4475_s2726_r7725_r7676_p2669',
    'mc15_13TeV.361605.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvvv_mll4.merge.DAOD_TOPQ1.e4054_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361606.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WWlvqq.merge.DAOD_TOPQ1.e4054_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361607.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqll_mll20.merge.DAOD_TOPQ1.e4054_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361608.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqvv.merge.DAOD_TOPQ1.e4054_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361609.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZlvqq_mqq20.merge.DAOD_TOPQ1.e4054_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361610.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.merge.DAOD_TOPQ1.e4054_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361611.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZvvqq_mqq20.merge.DAOD_TOPQ1.e4054_s2608_s2183_r7725_r7676_p2669',
]

###############################
### W->enu Sherpa
###############################

TopExamples.grid.Add('MyMC15c_Wenu_Sherpa').datasets = [
    'mc15_13TeV.361300.Sherpa_CT10_Wenu_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361301.Sherpa_CT10_Wenu_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361302.Sherpa_CT10_Wenu_Pt0_70_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361303.Sherpa_CT10_Wenu_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361304.Sherpa_CT10_Wenu_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361305.Sherpa_CT10_Wenu_Pt70_140_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361306.Sherpa_CT10_Wenu_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361307.Sherpa_CT10_Wenu_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361308.Sherpa_CT10_Wenu_Pt140_280_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361309.Sherpa_CT10_Wenu_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361310.Sherpa_CT10_Wenu_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361311.Sherpa_CT10_Wenu_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361312.Sherpa_CT10_Wenu_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361313.Sherpa_CT10_Wenu_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361314.Sherpa_CT10_Wenu_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361315.Sherpa_CT10_Wenu_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361316.Sherpa_CT10_Wenu_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361317.Sherpa_CT10_Wenu_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361318.Sherpa_CT10_Wenu_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361319.Sherpa_CT10_Wenu_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361320.Sherpa_CT10_Wenu_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361321.Sherpa_CT10_Wenu_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361322.Sherpa_CT10_Wenu_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361323.Sherpa_CT10_Wenu_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
]

###############################
### W->munu Sherpa
###############################
TopExamples.grid.Add('MyMC15c_Wmunu_Sherpa').datasets = [
    'mc15_13TeV.361324.Sherpa_CT10_Wmunu_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361325.Sherpa_CT10_Wmunu_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361326.Sherpa_CT10_Wmunu_Pt0_70_BFilter.merge.DAOD_TOPQ1.e3651_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361327.Sherpa_CT10_Wmunu_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361328.Sherpa_CT10_Wmunu_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361329.Sherpa_CT10_Wmunu_Pt70_140_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361330.Sherpa_CT10_Wmunu_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361331.Sherpa_CT10_Wmunu_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361332.Sherpa_CT10_Wmunu_Pt140_280_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361333.Sherpa_CT10_Wmunu_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361334.Sherpa_CT10_Wmunu_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361335.Sherpa_CT10_Wmunu_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361336.Sherpa_CT10_Wmunu_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361337.Sherpa_CT10_Wmunu_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361338.Sherpa_CT10_Wmunu_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361339.Sherpa_CT10_Wmunu_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361340.Sherpa_CT10_Wmunu_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361341.Sherpa_CT10_Wmunu_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361342.Sherpa_CT10_Wmunu_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361343.Sherpa_CT10_Wmunu_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361344.Sherpa_CT10_Wmunu_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361345.Sherpa_CT10_Wmunu_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361346.Sherpa_CT10_Wmunu_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361347.Sherpa_CT10_Wmunu_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
]
###############################
### W->taunu Sherpa
###############################
TopExamples.grid.Add('MyMC15c_Wtaunu_Sherpa').datasets = [
    'mc15_13TeV.361348.Sherpa_CT10_Wtaunu_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361349.Sherpa_CT10_Wtaunu_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361350.Sherpa_CT10_Wtaunu_Pt0_70_BFilter.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361351.Sherpa_CT10_Wtaunu_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361352.Sherpa_CT10_Wtaunu_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361353.Sherpa_CT10_Wtaunu_Pt70_140_BFilter.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361354.Sherpa_CT10_Wtaunu_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361355.Sherpa_CT10_Wtaunu_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361356.Sherpa_CT10_Wtaunu_Pt140_280_BFilter.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361357.Sherpa_CT10_Wtaunu_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361358.Sherpa_CT10_Wtaunu_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361359.Sherpa_CT10_Wtaunu_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361360.Sherpa_CT10_Wtaunu_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361361.Sherpa_CT10_Wtaunu_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361362.Sherpa_CT10_Wtaunu_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361363.Sherpa_CT10_Wtaunu_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361364.Sherpa_CT10_Wtaunu_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361365.Sherpa_CT10_Wtaunu_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361366.Sherpa_CT10_Wtaunu_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361367.Sherpa_CT10_Wtaunu_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361368.Sherpa_CT10_Wtaunu_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361369.Sherpa_CT10_Wtaunu_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361370.Sherpa_CT10_Wtaunu_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
    'mc15_13TeV.361371.Sherpa_CT10_Wtaunu_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7725_r7676_p2669',
]
###############################
### Z->ee Sherpa - Mll>40GeV
###############################
TopExamples.grid.Add('MyMC15c_Zee_Sherpa_Mllgt40').datasets = [
    'mc15_13TeV.361372.Sherpa_CT10_Zee_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361373.Sherpa_CT10_Zee_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361374.Sherpa_CT10_Zee_Pt0_70_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361375.Sherpa_CT10_Zee_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361376.Sherpa_CT10_Zee_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361377.Sherpa_CT10_Zee_Pt70_140_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361378.Sherpa_CT10_Zee_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361379.Sherpa_CT10_Zee_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361380.Sherpa_CT10_Zee_Pt140_280_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361381.Sherpa_CT10_Zee_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361382.Sherpa_CT10_Zee_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361383.Sherpa_CT10_Zee_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361384.Sherpa_CT10_Zee_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361385.Sherpa_CT10_Zee_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361386.Sherpa_CT10_Zee_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361387.Sherpa_CT10_Zee_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361388.Sherpa_CT10_Zee_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361389.Sherpa_CT10_Zee_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361390.Sherpa_CT10_Zee_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361391.Sherpa_CT10_Zee_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361392.Sherpa_CT10_Zee_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361393.Sherpa_CT10_Zee_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361394.Sherpa_CT10_Zee_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361395.Sherpa_CT10_Zee_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
]
###############################
### Z->mumu Sherpa - Mll>40GeV
###############################
TopExamples.grid.Add('MyMC15c_Zmumu_Sherpa_Mllgt40').datasets = [
    'mc15_13TeV.361396.Sherpa_CT10_Zmumu_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361397.Sherpa_CT10_Zmumu_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361398.Sherpa_CT10_Zmumu_Pt0_70_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361399.Sherpa_CT10_Zmumu_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361400.Sherpa_CT10_Zmumu_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361401.Sherpa_CT10_Zmumu_Pt70_140_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361402.Sherpa_CT10_Zmumu_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361403.Sherpa_CT10_Zmumu_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361404.Sherpa_CT10_Zmumu_Pt140_280_BFilter.merge.DAOD_TOPQ1.e3651_s2586_s2174_r7772_r7676_p2669',
    'mc15_13TeV.361405.Sherpa_CT10_Zmumu_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361406.Sherpa_CT10_Zmumu_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361407.Sherpa_CT10_Zmumu_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361408.Sherpa_CT10_Zmumu_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361409.Sherpa_CT10_Zmumu_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361410.Sherpa_CT10_Zmumu_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361411.Sherpa_CT10_Zmumu_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361412.Sherpa_CT10_Zmumu_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361413.Sherpa_CT10_Zmumu_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361414.Sherpa_CT10_Zmumu_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361415.Sherpa_CT10_Zmumu_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361416.Sherpa_CT10_Zmumu_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361417.Sherpa_CT10_Zmumu_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361418.Sherpa_CT10_Zmumu_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361419.Sherpa_CT10_Zmumu_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
]
###############################
### Z->tautau Sherpa - Mll>40GeV
###############################
TopExamples.grid.Add('MyMC15c_Ztautau_Sherpa_Mllgt40').datasets = [
    'mc15_13TeV.361420.Sherpa_CT10_Ztautau_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361420.Sherpa_CT10_Ztautau_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361421.Sherpa_CT10_Ztautau_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361422.Sherpa_CT10_Ztautau_Pt0_70_BFilter.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361423.Sherpa_CT10_Ztautau_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361424.Sherpa_CT10_Ztautau_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361425.Sherpa_CT10_Ztautau_Pt70_140_BFilter.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361426.Sherpa_CT10_Ztautau_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361427.Sherpa_CT10_Ztautau_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361428.Sherpa_CT10_Ztautau_Pt140_280_BFilter.merge.DAOD_TOPQ1.e3733_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361429.Sherpa_CT10_Ztautau_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361430.Sherpa_CT10_Ztautau_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361431.Sherpa_CT10_Ztautau_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361432.Sherpa_CT10_Ztautau_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361433.Sherpa_CT10_Ztautau_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361434.Sherpa_CT10_Ztautau_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361435.Sherpa_CT10_Ztautau_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361436.Sherpa_CT10_Ztautau_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361437.Sherpa_CT10_Ztautau_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361438.Sherpa_CT10_Ztautau_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361439.Sherpa_CT10_Ztautau_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361440.Sherpa_CT10_Ztautau_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361441.Sherpa_CT10_Ztautau_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361442.Sherpa_CT10_Ztautau_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361443.Sherpa_CT10_Ztautau_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4133_s2608_s2183_r7772_r7676_p2669',
]

###############################
### Z->ee Sherpa - 10GeV<Mll<40GeV
###############################
TopExamples.grid.Add('MyMC15c_Zee_Sherpa_Mlllt40').datasets = [
    'mc15_13TeV.361468.Sherpa_CT10_Zee_Mll10to40_Pt0_70_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361469.Sherpa_CT10_Zee_Mll10to40_Pt0_70_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361470.Sherpa_CT10_Zee_Mll10to40_Pt70_140_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361471.Sherpa_CT10_Zee_Mll10to40_Pt70_140_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361472.Sherpa_CT10_Zee_Mll10to40_Pt140_400_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361473.Sherpa_CT10_Zee_Mll10to40_Pt140_400_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361474.Sherpa_CT10_Zee_Mll10to40_Pt400_E_CMS_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361475.Sherpa_CT10_Zee_Mll10to40_Pt400_E_CMS_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
]

###############################
### Z->mumu Sherpa - 10GeV<Mll<40GeV
###############################
TopExamples.grid.Add('MyMC15c_Zmumu_Sherpa_Mlllt40').datasets = [
    'mc15_13TeV.361476.Sherpa_CT10_Zmumu_Mll10to40_Pt0_70_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361477.Sherpa_CT10_Zmumu_Mll10to40_Pt0_70_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361478.Sherpa_CT10_Zmumu_Mll10to40_Pt70_140_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361479.Sherpa_CT10_Zmumu_Mll10to40_Pt70_140_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361480.Sherpa_CT10_Zmumu_Mll10to40_Pt140_400_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361481.Sherpa_CT10_Zmumu_Mll10to40_Pt140_400_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361482.Sherpa_CT10_Zmumu_Mll10to40_Pt400_E_CMS_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361483.Sherpa_CT10_Zmumu_Mll10to40_Pt400_E_CMS_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
]

###############################
### Z->tautau Sherpa - 10GeV<Mll<40GeV
###############################
TopExamples.grid.Add('MyMC15c_Ztautau_Sherpa_Mlllt40').datasets = [
    'mc15_13TeV.361484.Sherpa_CT10_Ztautau_Mll10to40_Pt0_70_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361485.Sherpa_CT10_Ztautau_Mll10to40_Pt0_70_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361486.Sherpa_CT10_Ztautau_Mll10to40_Pt70_140_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361487.Sherpa_CT10_Ztautau_Mll10to40_Pt70_140_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361488.Sherpa_CT10_Ztautau_Mll10to40_Pt140_400_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361489.Sherpa_CT10_Ztautau_Mll10to40_Pt140_400_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361490.Sherpa_CT10_Ztautau_Mll10to40_Pt400_E_CMS_BVeto.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
    'mc15_13TeV.361491.Sherpa_CT10_Ztautau_Mll10to40_Pt400_E_CMS_BFilter.merge.DAOD_TOPQ1.e4198_s2608_s2183_r7772_r7676_p2669',
]

###############################
### W->enu Sherpa2.2
###############################
TopExamples.grid.Add('MyMC15c_Wenu_Sherpa2.2').datasets = [
    'mc15_13TeV.363460.Sherpa_NNPDF30NNLO_Wenu_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363461.Sherpa_NNPDF30NNLO_Wenu_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363462.Sherpa_NNPDF30NNLO_Wenu_Pt0_70_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363463.Sherpa_NNPDF30NNLO_Wenu_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363464.Sherpa_NNPDF30NNLO_Wenu_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363465.Sherpa_NNPDF30NNLO_Wenu_Pt70_140_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363466.Sherpa_NNPDF30NNLO_Wenu_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363467.Sherpa_NNPDF30NNLO_Wenu_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363468.Sherpa_NNPDF30NNLO_Wenu_Pt140_280_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363469.Sherpa_NNPDF30NNLO_Wenu_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363470.Sherpa_NNPDF30NNLO_Wenu_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363471.Sherpa_NNPDF30NNLO_Wenu_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363472.Sherpa_NNPDF30NNLO_Wenu_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363473.Sherpa_NNPDF30NNLO_Wenu_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363474.Sherpa_NNPDF30NNLO_Wenu_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4771_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363475.Sherpa_NNPDF30NNLO_Wenu_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363476.Sherpa_NNPDF30NNLO_Wenu_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363477.Sherpa_NNPDF30NNLO_Wenu_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363478.Sherpa_NNPDF30NNLO_Wenu_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363479.Sherpa_NNPDF30NNLO_Wenu_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363480.Sherpa_NNPDF30NNLO_Wenu_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363481.Sherpa_NNPDF30NNLO_Wenu_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363482.Sherpa_NNPDF30NNLO_Wenu_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7772_r7676_p2669', 
    'mc15_13TeV.363483.Sherpa_NNPDF30NNLO_Wenu_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
]

###############################
### W->munu Sherpa2.2
###############################
TopExamples.grid.Add('MyMC15c_Wmunu_Sherpa2.2').datasets = [
    'mc15_13TeV.363436.Sherpa_NNPDF30NNLO_Wmunu_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363437.Sherpa_NNPDF30NNLO_Wmunu_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363438.Sherpa_NNPDF30NNLO_Wmunu_Pt0_70_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363439.Sherpa_NNPDF30NNLO_Wmunu_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363440.Sherpa_NNPDF30NNLO_Wmunu_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363441.Sherpa_NNPDF30NNLO_Wmunu_Pt70_140_BFilter.merge.DAOD_TOPQ1.e4771_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363442.Sherpa_NNPDF30NNLO_Wmunu_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363443.Sherpa_NNPDF30NNLO_Wmunu_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363444.Sherpa_NNPDF30NNLO_Wmunu_Pt140_280_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363445.Sherpa_NNPDF30NNLO_Wmunu_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363446.Sherpa_NNPDF30NNLO_Wmunu_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363447.Sherpa_NNPDF30NNLO_Wmunu_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363448.Sherpa_NNPDF30NNLO_Wmunu_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363449.Sherpa_NNPDF30NNLO_Wmunu_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363450.Sherpa_NNPDF30NNLO_Wmunu_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363451.Sherpa_NNPDF30NNLO_Wmunu_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363452.Sherpa_NNPDF30NNLO_Wmunu_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363453.Sherpa_NNPDF30NNLO_Wmunu_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363454.Sherpa_NNPDF30NNLO_Wmunu_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363455.Sherpa_NNPDF30NNLO_Wmunu_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363456.Sherpa_NNPDF30NNLO_Wmunu_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363457.Sherpa_NNPDF30NNLO_Wmunu_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363458.Sherpa_NNPDF30NNLO_Wmunu_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363459.Sherpa_NNPDF30NNLO_Wmunu_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4715_s2726_r7725_r7676_p2669',
]

###############################
### W->taunu Sherpa2.2
###############################
TopExamples.grid.Add('MyMC15c_Wtaunu_Sherpa2.2').datasets = [
    'mc15_13TeV.363331.Sherpa_NNPDF30NNLO_Wtaunu_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363332.Sherpa_NNPDF30NNLO_Wtaunu_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363333.Sherpa_NNPDF30NNLO_Wtaunu_Pt0_70_BFilter.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363334.Sherpa_NNPDF30NNLO_Wtaunu_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363335.Sherpa_NNPDF30NNLO_Wtaunu_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363336.Sherpa_NNPDF30NNLO_Wtaunu_Pt70_140_BFilter.merge.DAOD_TOPQ1.e4779_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363337.Sherpa_NNPDF30NNLO_Wtaunu_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363338.Sherpa_NNPDF30NNLO_Wtaunu_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363339.Sherpa_NNPDF30NNLO_Wtaunu_Pt140_280_BFilter.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363340.Sherpa_NNPDF30NNLO_Wtaunu_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363341.Sherpa_NNPDF30NNLO_Wtaunu_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4779_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363342.Sherpa_NNPDF30NNLO_Wtaunu_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4779_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363343.Sherpa_NNPDF30NNLO_Wtaunu_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363344.Sherpa_NNPDF30NNLO_Wtaunu_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363345.Sherpa_NNPDF30NNLO_Wtaunu_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4779_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363346.Sherpa_NNPDF30NNLO_Wtaunu_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363347.Sherpa_NNPDF30NNLO_Wtaunu_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363348.Sherpa_NNPDF30NNLO_Wtaunu_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4779_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363349.Sherpa_NNPDF30NNLO_Wtaunu_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363350.Sherpa_NNPDF30NNLO_Wtaunu_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363351.Sherpa_NNPDF30NNLO_Wtaunu_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4779_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363352.Sherpa_NNPDF30NNLO_Wtaunu_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363353.Sherpa_NNPDF30NNLO_Wtaunu_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363354.Sherpa_NNPDF30NNLO_Wtaunu_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4709_s2726_r7725_r7676_p2669',
]

###############################
### Z->ee Sherpa2.2
###############################
TopExamples.grid.Add('MyMC15c_Zee_Sherpa2.2').datasets = [
    'mc15_13TeV.363388.Sherpa_NNPDF30NNLO_Zee_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363389.Sherpa_NNPDF30NNLO_Zee_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363390.Sherpa_NNPDF30NNLO_Zee_Pt0_70_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363391.Sherpa_NNPDF30NNLO_Zee_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363392.Sherpa_NNPDF30NNLO_Zee_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e4772_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363393.Sherpa_NNPDF30NNLO_Zee_Pt70_140_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363394.Sherpa_NNPDF30NNLO_Zee_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363395.Sherpa_NNPDF30NNLO_Zee_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363396.Sherpa_NNPDF30NNLO_Zee_Pt140_280_BFilter.merge.DAOD_TOPQ1.e4772_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363397.Sherpa_NNPDF30NNLO_Zee_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363398.Sherpa_NNPDF30NNLO_Zee_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363399.Sherpa_NNPDF30NNLO_Zee_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4772_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363400.Sherpa_NNPDF30NNLO_Zee_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363401.Sherpa_NNPDF30NNLO_Zee_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363402.Sherpa_NNPDF30NNLO_Zee_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363403.Sherpa_NNPDF30NNLO_Zee_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363404.Sherpa_NNPDF30NNLO_Zee_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363405.Sherpa_NNPDF30NNLO_Zee_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363406.Sherpa_NNPDF30NNLO_Zee_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363407.Sherpa_NNPDF30NNLO_Zee_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363408.Sherpa_NNPDF30NNLO_Zee_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4772_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363409.Sherpa_NNPDF30NNLO_Zee_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363410.Sherpa_NNPDF30NNLO_Zee_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363411.Sherpa_NNPDF30NNLO_Zee_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4772_s2726_r7725_r7676_p2669',
]

###############################
### Z->mumu Sherpa2.2
###############################
TopExamples.grid.Add('MyMC15c_Zmumu_Sherpa2.2').datasets = [
    'mc15_13TeV.363364.Sherpa_NNPDF30NNLO_Zmumu_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363365.Sherpa_NNPDF30NNLO_Zmumu_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363366.Sherpa_NNPDF30NNLO_Zmumu_Pt0_70_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363367.Sherpa_NNPDF30NNLO_Zmumu_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363368.Sherpa_NNPDF30NNLO_Zmumu_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363369.Sherpa_NNPDF30NNLO_Zmumu_Pt70_140_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363370.Sherpa_NNPDF30NNLO_Zmumu_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363371.Sherpa_NNPDF30NNLO_Zmumu_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363372.Sherpa_NNPDF30NNLO_Zmumu_Pt140_280_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363373.Sherpa_NNPDF30NNLO_Zmumu_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363374.Sherpa_NNPDF30NNLO_Zmumu_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363375.Sherpa_NNPDF30NNLO_Zmumu_Pt280_500_BFilter.merge.DAOD_TOPQ1.e4772_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363376.Sherpa_NNPDF30NNLO_Zmumu_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363377.Sherpa_NNPDF30NNLO_Zmumu_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363378.Sherpa_NNPDF30NNLO_Zmumu_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4772_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363379.Sherpa_NNPDF30NNLO_Zmumu_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363380.Sherpa_NNPDF30NNLO_Zmumu_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363381.Sherpa_NNPDF30NNLO_Zmumu_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363382.Sherpa_NNPDF30NNLO_Zmumu_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363383.Sherpa_NNPDF30NNLO_Zmumu_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363384.Sherpa_NNPDF30NNLO_Zmumu_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363385.Sherpa_NNPDF30NNLO_Zmumu_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363386.Sherpa_NNPDF30NNLO_Zmumu_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363387.Sherpa_NNPDF30NNLO_Zmumu_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4716_s2726_r7725_r7676_p2669',
]

###############################
### Z->tautau Sherpa2.2
###############################
TopExamples.grid.Add('MyMC15c_Ztautau_Sherpa2.2').datasets = [
    'mc15_13TeV.363361.Sherpa_NNPDF30NNLO_Ztautau_Pt0_70_CVetoBVeto.merge.DAOD_TOPQ1.e4689_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363362.Sherpa_NNPDF30NNLO_Ztautau_Pt0_70_CFilterBVeto.merge.DAOD_TOPQ1.e4689_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363363.Sherpa_NNPDF30NNLO_Ztautau_Pt0_70_BFilter.merge.DAOD_TOPQ1.e4743_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363102.Sherpa_NNPDF30NNLO_Ztautau_Pt70_140_CVetoBVeto.merge.DAOD_TOPQ1.e4742_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363103.Sherpa_NNPDF30NNLO_Ztautau_Pt70_140_CFilterBVeto.merge.DAOD_TOPQ1.e4742_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363104.Sherpa_NNPDF30NNLO_Ztautau_Pt70_140_BFilter.merge.DAOD_TOPQ1.e4792_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363105.Sherpa_NNPDF30NNLO_Ztautau_Pt140_280_CVetoBVeto.merge.DAOD_TOPQ1.e4666_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363106.Sherpa_NNPDF30NNLO_Ztautau_Pt140_280_CFilterBVeto.merge.DAOD_TOPQ1.e4666_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363107.Sherpa_NNPDF30NNLO_Ztautau_Pt140_280_BFilter.merge.DAOD_TOPQ1.e4742_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363108.Sherpa_NNPDF30NNLO_Ztautau_Pt280_500_CVetoBVeto.merge.DAOD_TOPQ1.e4666_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363109.Sherpa_NNPDF30NNLO_Ztautau_Pt280_500_CFilterBVeto.merge.DAOD_TOPQ1.e4792_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363111.Sherpa_NNPDF30NNLO_Ztautau_Pt500_700_CVetoBVeto.merge.DAOD_TOPQ1.e4666_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363112.Sherpa_NNPDF30NNLO_Ztautau_Pt500_700_CFilterBVeto.merge.DAOD_TOPQ1.e4742_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363113.Sherpa_NNPDF30NNLO_Ztautau_Pt500_700_BFilter.merge.DAOD_TOPQ1.e4742_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363114.Sherpa_NNPDF30NNLO_Ztautau_Pt700_1000_CVetoBVeto.merge.DAOD_TOPQ1.e4742_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363115.Sherpa_NNPDF30NNLO_Ztautau_Pt700_1000_CFilterBVeto.merge.DAOD_TOPQ1.e4792_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363116.Sherpa_NNPDF30NNLO_Ztautau_Pt700_1000_BFilter.merge.DAOD_TOPQ1.e4742_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363117.Sherpa_NNPDF30NNLO_Ztautau_Pt1000_2000_CVetoBVeto.merge.DAOD_TOPQ1.e4666_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363118.Sherpa_NNPDF30NNLO_Ztautau_Pt1000_2000_CFilterBVeto.merge.DAOD_TOPQ1.e4666_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363119.Sherpa_NNPDF30NNLO_Ztautau_Pt1000_2000_BFilter.merge.DAOD_TOPQ1.e4666_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363120.Sherpa_NNPDF30NNLO_Ztautau_Pt2000_E_CMS_CVetoBVeto.merge.DAOD_TOPQ1.e4690_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363121.Sherpa_NNPDF30NNLO_Ztautau_Pt2000_E_CMS_CFilterBVeto.merge.DAOD_TOPQ1.e4690_s2726_r7725_r7676_p2669',
    'mc15_13TeV.363122.Sherpa_NNPDF30NNLO_Ztautau_Pt2000_E_CMS_BFilter.merge.DAOD_TOPQ1.e4792_s2726_r7725_r7676_p2669',
]

###############################
### Pythia8B bb->mu 
###############################
TopExamples.grid.Add('MyMC15c_Pythia8Bbbmu ').datasets = [
    'mc15_13TeV.361250.Pythia8B_A14_NNPDF23LO_bbTomu15.merge.DAOD_TOPQ1.e3878_s2608_s2183_r7772_r7676_p2771',
    'mc15_13TeV.304697.Pythia8BEvtGen_A14NNPDF23LO_pp_JpsimumuZmumu.merge.DAOD_TOPQ1.e4672_a766_r7772_r7676_p2669',
    'mc15_13TeV.304697.Pythia8BEvtGen_A14NNPDF23LO_pp_JpsimumuZmumu.merge.DAOD_TOPQ1.e4672_s2726_r7772_r7676_p2669',
    'mc15_13TeV.304698.Pythia8BEvtGen_A14NNPDF23LO_pp_JpsimumuZee.merge.DAOD_TOPQ1.e4672_a766_r7772_r7676_p2669',
    'mc15_13TeV.304699.Pythia8BEvtGen_A14NNPDF23LO_pp_UpsimumuZmumu.merge.DAOD_TOPQ1.e4672_a766_r7772_r7676_p2669',
    'mc15_13TeV.304700.Pythia8BEvtGen_A14NNPDF23LO_pp_UpsimumuZee.merge.DAOD_TOPQ1.e4672_a766_r7772_r7676_p2669',
    #'mc15_13TeV.304701.Pythia8BEvtGen_A14NNPDF23LO_pp_JpsieeZmumu.merge.DAOD_TOPQ1.e4672_a766_r7772_r7676_p2669',
    #'mc15_13TeV.304702.Pythia8BEvtGen_A14NNPDF23LO_pp_JpsieeZee.merge.DAOD_TOPQ1.e4672_a766_r7772_r7676_p2669',
    #'mc15_13TeV.304702.Pythia8BEvtGen_A14NNPDF23LO_pp_JpsieeZee.merge.DAOD_TOPQ1.e4672_s2726_r7772_r7676_p2669',
    #'mc15_13TeV.304703.Pythia8BEvtGen_A14NNPDF23LO_pp_UpsieeZmumu.merge.DAOD_TOPQ1.e4672_a766_r7772_r7676_p2669',
    #'mc15_13TeV.304704.Pythia8BEvtGen_A14NNPDF23LO_pp_UpsieeZee.merge.DAOD_TOPQ1.e4672_a766_r7772_r7676_p2669', 
]
