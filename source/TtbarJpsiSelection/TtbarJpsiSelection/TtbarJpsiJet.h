#ifndef TTBARJPSISELECTION_TTBARJPSIJET_H
#define TTBARJPSISELECTION_TTBARJPSIJET_H
///////////////////////////////////////////////////////////////////////////
// AUTHOR     Frédéric Derue (LPNHE Paris, derue@lpnhe.in2p3.fr)
// DEVELOPPER 
//            Frédéric Derue (LPNHE Paris)

// PURPOSE    Custom jet selection
//            code evolved from https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/PhysAnalysis/Run2/HowtoExtendAnalysisTop/trunk/HowtoExtendAnalysisTop
//
// UPDATE     18/07/2016 creation of the package (from existing private code)
//
/////////////////////////////////////////////////////////////////////////

#include "TopObjectSelectionTools/JetSelectionBase.h"

namespace top 
{
  /**
   * @brief Select jet with a pt min and a pt max
   *        No physics reason, demonstration of a plug-in
   */
  class TtbarJpsiJet : public JetSelectionBase {
  public:
    TtbarJpsiJet(const double ptMin,const double ptMax,const double etamax, const double jvtmin);
    virtual ~TtbarJpsiJet(){}
    
    virtual bool passSelection(const xAOD::Jet& jet) const override;
    
    ///Print the cuts to the ostream.
    virtual void print(std::ostream& os) const override;
    
  protected: 
    ///The lower pT cut threshold to apply to the object.
    double m_ptMin;
    ///The upper pT cut threshold to apply to the object.
    double m_ptMax;      
    
    ///The upper eta cut.
    double m_etamax;
    
    ///The minimum JVT cut for jets with pT < 50 GeV and |eta| < 2.4.
    bool m_applyJVTCut;
    double m_jvtmin;      
  };
}
#endif
