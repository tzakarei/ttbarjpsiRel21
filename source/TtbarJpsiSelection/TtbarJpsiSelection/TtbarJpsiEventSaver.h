#ifndef TTBARJPSISELECTION_TTBARJPSIEVENTSAVER_H
#define TTBARJPSISELECTION_TTBARJPSIEVENTSAVER_H
///////////////////////////////////////////////////////////////////////////
// AUTHOR     Frédéric Derue (LPNHE Paris, derue@lpnhe.in2p3.fr)
// DEVELOPPER 
//            Frédéric Derue (LPNHE Paris)

// PURPOSE    Event saver for ttbar+Jpsi selection
//            code evolved from https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/PhysAnalysis/Run2/HowtoExtendAnalysisTop/trunk/HowtoExtendAnalysisTop
//            and https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/PhysAnalysis/Run2/TopFakesUtils/trunk/Root/TopFakesEventSaver.cxx
//
// UPDATE     18/07/2016 creation of the package (from existing private code)
//            20/07/2016 save separately standard muons and additional ones
//            26/10/2016 add d0/z0, run with AT-2.4.19
//
/////////////////////////////////////////////////////////////////////////

#include "xAODBPhys/BPhysHelper.h"
#include "TopAnalysis/EventSaverFlatNtuple.h"
// for trigger prescales 
#include "TrigDecisionTool/TrigDecisionTool.h"
// for isolation flags
#include "TopObjectSelectionTools/IsolationTools.h"
// for muon quality flags
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"


#include "AsgTools/IAsgTool.h"
#include "xAODBase/IParticle.h"
#include "AsgTools/AnaToolHandle.h"

#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
#include "MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h"

#include "TriggerMatchingTool/MatchingTool.h"
#include "PileupReweighting/PileupReweightingTool.h"



/**
 * @brief Event saver for TtbarJpsiSelection Flat Ntuples
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */
namespace top {
  class TtbarJpsiEventSaver : public top::EventSaverFlatNtuple {
  public:
    ///-- Default constructor with no arguments - needed for ROOT --///
    TtbarJpsiEventSaver();
    ///-- Destructor does nothing --///
    virtual ~TtbarJpsiEventSaver();
    
    ///-- initialize function for top::EventSaverFlatNtuple --///
    ///-- We will be setting up out custom variables here --///
    virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;
    
    ///-- Keep the asg::AsgTool happy --///
    virtual StatusCode initialize() override{return StatusCode::SUCCESS;} 
    
    ///-- saveEvent function for top::EventSaverFlatNtuple --///
    ///-- We will be setting our custom variables on a per-event basis --///
    virtual void saveEvent(const top::Event& event) override;

    virtual const xAOD::EventInfo* eventInfo_f() const ;

    
  private:
    //to hold the configuration
    std::shared_ptr<top::TopConfig> m_config;    
    //trigger tools
    ToolHandle<Trig::TrigDecisionTool> m_trigDecisionTool;
        // for muon quality
    ToolHandle<CP::IMuonSelectionTool> m_muonsel;
    
    CP::MuonSelectionTool * m_muonSelection; //!
    CP::MuonCalibrationAndSmearingTool * m_muonCalibTool; //!
    CP::MuonEfficiencyScaleFactors * m_muonEfficiency; //!

    void AddVertex(const top::Event& event);
    void AddElectrons(const top::Event& event);
    void AddMuons(const top::Event& event);
    void AddJets(const top::Event& event); 
    void AddMET(const top::Event& event); 
    void AddMuonsForJpsi(const top::Event& event);
    void AddJpsi(const top::Event& event);
    void AddJpsiTruth(const top::Event& event);
    void AddTriggerPrescales(); 

    ///-- Some additional custom variables for the output --///
    //event info
    unsigned int m_lumiBlock;
    unsigned int m_npvtx;

    // primary vertex
    float m_vtxz;
    
    // Electron variables
    unsigned int m_el_n;
    std::vector<int>    m_el_trigMatch;
    
    // Muon variables
    unsigned int       m_mu_n;
    std::vector<int>   m_mu_trigMatch;
    std::vector<float> m_mu_d0;
    std::vector<float> m_mu_z0;
    
    // Muon variables for J/psi candidates
    unsigned int       m_mu_jpsi_n;
    std::vector<int>   m_mu_jpsi_author;
    std::vector<float> m_mu_jpsi_pt;
    std::vector<float> m_mu_jpsi_eta;
    std::vector<float> m_mu_jpsi_phi;
    std::vector<float> m_mu_jpsi_charge;
    std::vector<float> m_mu_jpsi_d0;
    std::vector<float> m_mu_jpsi_z0;
    std::vector<float> m_mu_jpsi_d0sig;
    std::vector<float> m_mu_jpsi_delta_z0_sintheta;
    std::vector<int>   m_mu_jpsi_overlap;
    std::vector<bool>  m_mu_jpsi_Tight;
    std::vector<bool>  m_mu_jpsi_Medium;
    std::vector<bool>  m_mu_jpsi_Loose;
    std::vector<bool>  m_mu_jpsi_VeryLoose;
    std::vector<float> m_mu_jpsi_topoetcone20;
    std::vector<float> m_mu_jpsi_topoetcone30;
    std::vector<float> m_mu_jpsi_topoetcone40;
    std::vector<float> m_mu_jpsi_ptvarcone20;
    std::vector<float> m_mu_jpsi_ptvarcone30;
    std::vector<float> m_mu_jpsi_ptvarcone40;
    std::vector<int>   m_mu_jpsi_true_type;
    std::vector<int>   m_mu_jpsi_true_origin;

    // J/psi candidates (from BPHY1)
    unsigned int       m_jpsi_n;
/*    std::vector<float> m_jpsiMass;
    std::vector<float> m_jpsiMassError;
    std::vector<float> m_jpsiMassRec;
    std::vector<float> m_jpsiMassPullRec;
    std::vector<float> m_jpsiMassPullMC;
    std::vector<float> m_jpsiChi2;

    // refitted tracks (from BPHY1)
    std::vector<float> m_trkRefitPx1;
    std::vector<float> m_trkRefitPy1;
    std::vector<float> m_trkRefitPz1;
    std::vector<float> m_trkRefitPx2;
    std::vector<float> m_trkRefitPy2;
    std::vector<float> m_trkRefitPz2;
*/

    std::vector<double> miT_PV_MaxSumPt2_Z;
    std::vector<double> miT_PV_Min_A0_Z;
    std::vector<double> miT_PV_Min_Z0_Z;
 
    std::vector<double> miT_DiMuon_Mass;
    std::vector<double> miT_DiMuon_Mass_Err;
 
    std::vector<double> miT_DiMuon_Vtxx;
    std::vector<double> miT_DiMuon_Vtxy;
    std::vector<double> miT_DiMuon_Vtxz;

    std::vector<double> miT_DiMuon_Pt;
    std::vector<double> miT_DiMuon_Rapidity;
    std::vector<double> miT_DiMuon_Eta;
    std::vector<double> miT_DiMuon_Phi;
 
    std::vector<double> miT_MuPlus_Track_Pt;
    std::vector<double> miT_MuPlus_Track_Eta;
    std::vector<double> miT_MuPlus_Track_Phi;
    std::vector<int> miT_MuPlus_Tight;
    std::vector<double> miT_MuPlus_RecoEff;
 
    std::vector<double> miT_MuMinus_Track_Pt;
    std::vector<double> miT_MuMinus_Track_Eta;
    std::vector<double> miT_MuMinus_Track_Phi;
    std::vector<int> miT_MuMinus_Tight;
    std::vector<double> miT_MuMinus_RecoEff;
 
    std::vector<double> miT_Tau_InvMass_MaxSumPt2;
    std::vector<double> miT_Tau_JpsiMass_MaxSumPt2;
 
    std::vector<double> miT_Tau_InvMass_Min_A0;
    std::vector<double> miT_Tau_JpsiMass_Min_A0;
 
    std::vector<double> miT_Tau_InvMass_Min_Z0;
    std::vector<double> miT_Tau_JpsiMass_Min_Z0;
 
    std::vector<double> miT_TauErr_InvMass_MaxSumPt2;
    std::vector<double> miT_TauErr_JpsiMass_MaxSumPt2;
 
    std::vector<double> miT_TauErr_InvMass_Min_A0;
    std::vector<double> miT_TauErr_JpsiMass_Min_A0;
 
    std::vector<double> miT_TauErr_InvMass_Min_Z0;
    std::vector<double> miT_TauErr_JpsiMass_Min_Z0;

    std::vector<double> miT_Lxy_Min_A0;
    std::vector<double> miT_LxyErr_Min_A0;

    std::vector<double> miT_Lxy_MaxSumPt2;
    std::vector<double> miT_LxyErr_MaxSumPt2;

    std::vector<double> miT_Lxy_Min_Z0;
    std::vector<double> miT_LxyErr_Min_Z0;
  
    std::vector<double> miT_Z0_Min_A0;
    std::vector<double> miT_Z0Err_Min_A0;

    std::vector<double> miT_Z0_Min_Z0;
    std::vector<double> miT_Z0Err_Min_Z0;

    std::vector<double> miT_Z0_MaxSumPt2;
    std::vector<double> miT_Z0Err_MaxSumPt2;
    
    std::vector<double> miT_A0_Min_A0;
    std::vector<double> miT_A0Err_Min_A0;

    std::vector<double> miT_A0_Min_Z0;
    std::vector<double> miT_A0Err_Min_Z0;

    std::vector<double> miT_A0_MaxSumPt2;
    std::vector<double> miT_A0Err_MaxSumPt2;

    std::vector<double> miT_A0xy_Min_A0;
    std::vector<double> miT_A0xyErr_Min_A0;

    std::vector<double> miT_A0xy_Min_Z0;
    std::vector<double> miT_A0xyErr_Min_Z0;

    std::vector<double> miT_A0xy_MaxSumPt2;
    std::vector<double> miT_A0xyErr_MaxSumPt2;

    std::vector<double> miT_pvx_ref_z_MaxSumPt2;
    std::vector<double> miT_pvx_orig_z_MaxSumPt2;

    std::vector<int> miT_TruthMatch;

    std::vector<double> miT_Truth_Jpsi_Pt;
    std::vector<double> miT_Truth_Jpsi_Eta;
    std::vector<double> miT_Truth_Jpsi_Phi;
    std::vector<double> miT_Truth_Jpsi_Mass;

    std::vector<double> miT_Truth_MuPlus_Pt;
    std::vector<double> miT_Truth_MuPlus_Eta;
    std::vector<double> miT_Truth_MuPlus_Phi;

    std::vector<double> miT_Truth_MuMinus_Pt;
    std::vector<double> miT_Truth_MuMinus_Eta;
    std::vector<double> miT_Truth_MuMinus_Phi;
  
    std::vector<double> miT_Truth_PV_Z;

    std::vector<int> miT_isGood;
    std::vector<int> miT_Muon_Author;

    // Angles
    std::vector<float> m_Muon_Phi_hx         ;              //!
    std::vector<float> m_Muon_CosTheta_hx    ;         //!
    std::vector<float> m_Muon_Phi_hxOLD      ;              //!
    std::vector<float> m_Muon_CosTheta_hxOLD ;         //!

    // Reco Eff. flags
    std::vector<float> m_Muon0_RecoEffTight  ;       //!
    std::vector<float> m_Muon1_RecoEffTight  ;       //!
    std::vector<float> m_Muon0_RecoEffMedium ;       //!
    std::vector<float> m_Muon1_RecoEffMedium ;       //!
    std::vector<float> m_Muon0_RecoEffLoose  ;       //!
    std::vector<float> m_Muon1_RecoEffLoose  ;       //!
    std::vector<float> m_Muon0_RecoEffLowPt  ;       //!
    std::vector<float> m_Muon1_RecoEffLowPt  ;       //!

    //muon quality flags
    std::vector<int>   m_Muon0_Quality ;            //!
    std::vector<int>   m_Muon1_Quality ;            //!


    // MUONS
    
    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool_loose;
    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool_medium;
    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool_tight;
    asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool_lowpt;
    
    asg::AnaToolHandle<CP::IMuonCalibrationAndSmearingTool> m_muonCalibSmearTool;

    asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors>     m_muonEffSFTightTool;
    asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors>     m_muonEffSFMediumTool;
    asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors>     m_muonEffSFLooseTool;
    asg::AnaToolHandle<CP::IMuonEfficiencyScaleFactors>     m_muonEffSFLowPtTool;

/*
    // vertex (check if it is not equal to m_vtxz) (from BPHY1)
    std::vector<float> m_vx;
    std::vector<float> m_vy;
    std::vector<float> m_vz;

    // original track (should be equal to m_mu_jpsi_ ?) (from BPHY1)
    std::vector<float> m_trkOrigCharge1;
    std::vector<float> m_trkOrigPx1;
    std::vector<float> m_trkOrigPy1;
    std::vector<float> m_trkOrigPz1;
    std::vector<float> m_trkOrigCharge2;
    std::vector<float> m_trkOrigPx2;
    std::vector<float> m_trkOrigPy2;
    std::vector<float> m_trkOrigPz2;
*/
    // Jets variables
    unsigned int m_jet_n;
    std::vector<float>  m_jet_m;
    
    // MET variables,Ht
    float m_met_px;
    float m_met_py;
    float m_met_sumet;
        
    const float m_MuonMass; //!

    // for trigger prescales 
    std::unordered_map<std::string, float> m_triggerPrescales;
    
    // static function that prevents the unwanted creation of branches in any eventsaver using the tree manager
    static int getBranchStatus(top::TreeManager const *, std::string const & variableName);
    
    virtual StatusCode passMuonLoose (const xAOD::Muon*, bool& isPassed) const;
    virtual StatusCode passMuonMedium(const xAOD::Muon*, bool& isPassed) const;
    virtual StatusCode passMuonTight (const xAOD::Muon*, bool& isPassed) const;
    virtual StatusCode passMuonLowPt (const xAOD::Muon*, bool& isPassed) const;

    virtual std::unique_ptr<xAOD::Muon> makeCalibMuon(const xAOD::Muon*) const;

    virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFTightTool()  const ;
    virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFMediumTool() const ;
    virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFLooseTool()  const ;
    virtual const CP::IMuonEfficiencyScaleFactors* muonEffSFLowPtTool()  const ;


    virtual  bool VatoHelicity(const TLorentzVector & mupl,
                              const TLorentzVector & mumi,
                              TVector3 & HelicityAxis,
                              TVector3 & xAxis,
                              TVector3 & yAxis,
                              double & cosTheta_mupl,
                              double & cosTheta_mumi,
                              double & phi_mupl,
                               double & phi_mumi) const ;

    virtual  bool VatoHelicity(const TLorentzVector & mupl, const TLorentzVector & mumi,
                               double & cosTheta_mupl,double & phi_mupl) const ; // shorthand of above


    // standard
    virtual double cosMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const ;
    virtual double phiMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const ;

    ///-- Tell RootCore to build a dictionary (we need this) --///
    ClassDef(top::TtbarJpsiEventSaver, 0);
  };
}

#endif
