#ifndef TTBARJPSISELECTION_TTBARJPSIOBJECTLOADER_H
#define TTBARJPSISELECTION_TTBARJPSIOBJECTLOADER_H
///////////////////////////////////////////////////////////////////////////
// AUTHOR     Frédéric Derue (LPNHE Paris, derue@lpnhe.in2p3.fr)
// DEVELOPPER 
//            Frédéric Derue (LPNHE Paris)

// PURPOSE    Object loader for ttbar+Jpsi selection
//            code evolved from https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/PhysAnalysis/Run2/HowtoExtendAnalysisTop/trunk/HowtoExtendAnalysisTop
//            and https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/PhysAnalysis/Run2/TopFakesUtils/trunk/Root/TopFakesObjectLoader.cxx
//
// UPDATE     18/07/2016 creation of the package (from existing private code)
//
/////////////////////////////////////////////////////////////////////////

#include "TopAnalysis/ObjectLoaderBase.h"

namespace top {

  /**
   * @brief This loads the "standard" object selection used in the top group, plus a few modifications if needed.
  *
  */
  class TtbarJpsiObjectLoader : public ObjectLoaderBase {
  public:
    
    top::TopObjectSelection* init(std::shared_ptr<top::TopConfig> topConfig);
    
    ClassDef(top::TtbarJpsiObjectLoader, 0)
      };
  
}

#endif
