///////////////////////////////////////////////////////////////////////////
// AUTHOR     Frédéric Derue (LPNHE Paris, derue@lpnhe.in2p3.fr) 
// DEVELOPPER 
//            Frédéric Derue (LPNHE Paris)

// PURPOSE    Event saver for ttbar+Jpsi selection
//            code evolved from https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/PhysAnalysis/Run2/HowtoExtendAnalysisTop/trunk/HowtoExtendAnalysisTop
//            and https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/PhysAnalysis/Run2/TopFakesUtils/trunk/Root/TopFakesEventSaver.cxx
//
// UPDATE     18/07/2016 creation of the package (from existing private code)
//            20/07/2016 save separately standard muons and additional ones
//            21/07/2016 fixes to run on AT-2.4.16
//            26/10/2016 add d0/z0, run with AT-2.4.19
//
/////////////////////////////////////////////////////////////////////////

#include "TopEvent/Event.h"
#include "TopEvent/EventTools.h"
#include "TopConfiguration/TopConfig.h"
#include "TopEventSelectionTools/TreeManager.h"


#include "xAODTruth/xAODTruthHelpers.h" 

///-- The custom event selections we have defined in this package --///
#include "TtbarJpsiSelection/TtbarJpsiEventSaver.h"
#include "TtbarJpsiSelection/TtbarJpsixAODUtils.h"

#include "MuonSelectorTools/MuonSelectionTool.h"


#define XXX std::cout << "I am here: " << __FILE__ << ":" << __LINE__ << std::endl;

namespace top{
  ///-- Constructor --///
  TtbarJpsiEventSaver::TtbarJpsiEventSaver() : 
    top::EventSaverFlatNtuple(),
    m_config(nullptr),
    m_trigDecisionTool("Trig::TrigDecisionTool"),
    m_muonsel("CP::MuonSelectionTool"),
    m_lumiBlock(0),
    m_npvtx(0),
    m_vtxz(0.),
    m_el_n(0),
    m_mu_n(0),
    m_mu_jpsi_n(0),
    m_jpsi_n(0),
    m_jet_n(0),
    m_met_px(0.),
    m_met_py(0.),
    m_met_sumet(0.),
    m_MuonMass(105.658),// MeV
    m_muonSelectionTool_loose ("CP::MuonSelectionTool/ToolBoxMuonSelectionToolLoose"),
    m_muonSelectionTool_medium("CP::MuonSelectionTool/ToolBoxMuonSelectionToolMedium"),
    m_muonSelectionTool_tight ("CP::MuonSelectionTool/ToolBoxMuonSelectionToolTight"),
    m_muonSelectionTool_lowpt ("CP::MuonSelectionTool/ToolBoxMuonSelectionToolLowPt"),
    m_muonCalibSmearTool      ("CP::MuonCalibrationAndSmearingTool/ToolBoxMuonCalibrationAndSmearingTool" ),
    m_muonEffSFTightTool      ("CP::MuonEfficiencyScaleFactors/ToolBoxMuonEfficiencyScaleFactorsTightTool"),
    m_muonEffSFMediumTool     ("CP::MuonEfficiencyScaleFactors/ToolBoxMuonEfficiencyScaleFactorsMediumTool"),
    m_muonEffSFLooseTool      ("CP::MuonEfficiencyScaleFactors/ToolBoxMuonEfficiencyScaleFactorsLooseTool"),
    m_muonEffSFLowPtTool      ("CP::MuonEfficiencyScaleFactors/ToolBoxMuonEfficiencyScaleFactorsLowPtTool")
  {
    branchFilters().push_back(std::bind(&getBranchStatus, std::placeholders::_1, std::placeholders::_2));
    m_muonSelectionTool_loose .declarePropertyFor( this, "MuonSelectionToolLoose" , "Loose" );
    m_muonSelectionTool_medium.declarePropertyFor( this, "MuonSelectionToolMedium", "Medium");
    m_muonSelectionTool_tight .declarePropertyFor( this, "MuonSelectionToolTight" , "Tight" );
    m_muonSelectionTool_lowpt .declarePropertyFor( this, "MuonSelectionToolLowPt" , "LowPt" );

    m_muonCalibSmearTool.declarePropertyFor( this, "ToolBoxMuonCalibrationAndSmearingTool" , "calib" );

    m_muonEffSFTightTool  .declarePropertyFor( this, "ToolBoxMuonEfficiencyScaleFactorsTightTool" , "muonEffs" );
    m_muonEffSFMediumTool .declarePropertyFor( this, "ToolBoxMuonEfficiencyScaleFactorsMediumTool" , "muonEffs" );
    m_muonEffSFLooseTool  .declarePropertyFor( this, "ToolBoxMuonEfficiencyScaleFactorsLooseTool" , "muonEffs" );
    m_muonEffSFLowPtTool  .declarePropertyFor( this, "ToolBoxMuonEfficiencyScaleFactorsLowPtTool" , "muonEffs" );

  }

  TtbarJpsiEventSaver::~TtbarJpsiEventSaver()
  {
//    delete m_muonSelection;
//    delete m_muonCalibTool;
//    delete m_muonEfficiency;
    //std::cout<<"deleted"<<std::endl;
  }
  
  // ===============================================================
  ///-- initialize - done once at the start of a job before the loop over events --///
  void TtbarJpsiEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
  {
    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);
    m_config = config;
    
    ///-- Loop over the systematic TTrees and add the custom variables --///
    for (auto systematicTree : treeManagers()) {
      // event info
      systematicTree->makeOutputVariable(m_lumiBlock, "lbn");
      systematicTree->makeOutputVariable(m_vtxz,      "Vtxz");
      systematicTree->makeOutputVariable(m_npvtx,     "npVtx");
      
      // electrons (for top)
      systematicTree->makeOutputVariable(m_el_n,         "el_n");
      systematicTree->makeOutputVariable(m_el_trigMatch, "el_trigMatch");
      
      // muons (for top)
      systematicTree->makeOutputVariable(m_mu_n,         "mu_n");
      systematicTree->makeOutputVariable(m_mu_trigMatch, "mu_trigMatch");
      systematicTree->makeOutputVariable(m_mu_d0,         "mu_d0");
      systematicTree->makeOutputVariable(m_mu_z0,         "mu_z0");

      // jets (for top)
      systematicTree->makeOutputVariable(m_jet_n, "jet_n");
      systematicTree->makeOutputVariable(m_jet_m, "jet_m");

      // met, Ht
      systematicTree->makeOutputVariable(m_met_px,    "met_px");
      systematicTree->makeOutputVariable(m_met_py,    "met_py");
      systematicTree->makeOutputVariable(m_met_sumet, "met_sumet");

      // muons for J/psi candidates
      systematicTree->makeOutputVariable(m_mu_jpsi_n, "mu_jpsi_n");
      systematicTree->makeOutputVariable(m_mu_jpsi_author,    
					 "mu_jpsi_author");
      systematicTree->makeOutputVariable(m_mu_jpsi_pt,        
					 "mu_jpsi_pt");
      systematicTree->makeOutputVariable(m_mu_jpsi_eta,       
					 "mu_jpsi_eta");
      systematicTree->makeOutputVariable(m_mu_jpsi_phi,       
					 "mu_jpsi_phi");
      systematicTree->makeOutputVariable(m_mu_jpsi_charge,       
					 "mu_jpsi_charge");
      systematicTree->makeOutputVariable(m_mu_jpsi_d0,       
					 "mu_jpsi_d0");
      systematicTree->makeOutputVariable(m_mu_jpsi_z0,       
					 "mu_jpsi_z0");
      systematicTree->makeOutputVariable(m_mu_jpsi_d0sig,       
					 "mu_jpsi_d0sig");
      systematicTree->makeOutputVariable(m_mu_jpsi_delta_z0_sintheta,       
					 "mu_jpsi_delta_z0_sintheta");
      // to check if it passes overlap removal
      systematicTree->makeOutputVariable(m_mu_jpsi_overlap,   
					 "mu_jpsi_overlap");
      // pid variables
      systematicTree->makeOutputVariable(m_mu_jpsi_Tight,     
					 "mu_jpsi_Tight" );
      systematicTree->makeOutputVariable(m_mu_jpsi_Medium,    
					 "mu_jpsi_Medium" );
      systematicTree->makeOutputVariable(m_mu_jpsi_Loose,     
					 "mu_jpsi_Loose" );
      systematicTree->makeOutputVariable(m_mu_jpsi_VeryLoose, 
					 "mu_jpsi_VeryLoose" );
      // isolation variables
      systematicTree->makeOutputVariable(m_mu_jpsi_topoetcone20, 
					 "mu_jpsi_topoetcone20");
      systematicTree->makeOutputVariable(m_mu_jpsi_topoetcone30, 
					 "mu_jpsi_topoetcone30");
      systematicTree->makeOutputVariable(m_mu_jpsi_topoetcone40, 
					 "mu_jpsi_topoetcone40");
      systematicTree->makeOutputVariable(m_mu_jpsi_ptvarcone20,     
					 "mu_jpsi_ptvarcone20");
      systematicTree->makeOutputVariable(m_mu_jpsi_ptvarcone30,     
					 "mu_jpsi_ptvarcone30");
      systematicTree->makeOutputVariable(m_mu_jpsi_ptvarcone40,     
					 "mu_jpsi_ptvarcone40");
      // truth level
      if(m_config->isMC()) {
	      systematicTree->makeOutputVariable(m_mu_jpsi_true_type,     
					   "mu_jpsi_true_type");
	      systematicTree->makeOutputVariable(m_mu_jpsi_true_origin,     
					   "mu_jpsi_true_origin");
      }
/*
      // J/psi candidates (from BPHY1)
      systematicTree->makeOutputVariable(m_jpsi_n, "jpsi_n");
      systematicTree->makeOutputVariable(m_jpsiMass,"jpsi_mass");
      systematicTree->makeOutputVariable(m_jpsiMassError,"jpsi_massError");
      systematicTree->makeOutputVariable(m_jpsiMassRec,"jpsi_massRec");
      systematicTree->makeOutputVariable(m_jpsiMassPullRec,"jpsi_massPullRec");
      systematicTree->makeOutputVariable(m_jpsiMassPullMC,"jpsi_massPullMC");
      systematicTree->makeOutputVariable(m_jpsiChi2,"jpsi_chi2");

      // refitted tracks  (from BPHY1)
      systematicTree->makeOutputVariable(m_trkRefitPx1,"trkRefitPx1");
      systematicTree->makeOutputVariable(m_trkRefitPy1,"trkRefitPy1");
      systematicTree->makeOutputVariable(m_trkRefitPz1,"trkRefitPz1");
      systematicTree->makeOutputVariable(m_trkRefitPx2,"trkRefitPx2");
      systematicTree->makeOutputVariable(m_trkRefitPy2,"trkRefitPy2");
      systematicTree->makeOutputVariable(m_trkRefitPz2,"trkRefitPz2");
*/
      //J/psi
      systematicTree->makeOutputVariable(miT_DiMuon_Mass,"DiMuon_Mass");      
      systematicTree->makeOutputVariable(miT_DiMuon_Mass_Err,"DiMuon_Mass_Err"); 

      systematicTree->makeOutputVariable(miT_DiMuon_Vtxx,"DiMuon_Vtxx");
      systematicTree->makeOutputVariable(miT_DiMuon_Vtxy,"DiMuon_Vtxy");     
      systematicTree->makeOutputVariable(miT_DiMuon_Vtxz,"DiMuon_Vtxz");

      systematicTree->makeOutputVariable(miT_DiMuon_Pt,"DiMuon_Pt");      
      systematicTree->makeOutputVariable(miT_DiMuon_Rapidity,"DiMuon_Rapidity");      
      systematicTree->makeOutputVariable(miT_DiMuon_Eta,"DiMuon_Eta");      
      systematicTree->makeOutputVariable(miT_DiMuon_Phi,"DiMuon_Phi");      

      systematicTree->makeOutputVariable(miT_MuPlus_Track_Pt,"MuPlus_Track_Pt");
      systematicTree->makeOutputVariable(miT_MuPlus_Track_Eta,"MuPlus_Track_Eta");
      systematicTree->makeOutputVariable(miT_MuPlus_Track_Phi,"MuPlus_Track_Phi");
      systematicTree->makeOutputVariable(miT_MuPlus_Tight,"MuPlus_Tight");
      systematicTree->makeOutputVariable(miT_MuPlus_RecoEff,"MuPlus_RecoEff");

      systematicTree->makeOutputVariable(miT_MuMinus_Track_Pt,"MuMinus_Track_Pt");
      systematicTree->makeOutputVariable(miT_MuMinus_Track_Eta,"MuMinus_Track_Eta");
      systematicTree->makeOutputVariable(miT_MuMinus_Track_Phi,"MuMinus_Track_Phi");
      systematicTree->makeOutputVariable(miT_MuMinus_Tight,"MuMinus_Tight");
      systematicTree->makeOutputVariable(miT_MuMinus_RecoEff,"MuMinus_RecoEff");

      systematicTree->makeOutputVariable(miT_Tau_InvMass_MaxSumPt2,"Tau_InvMass_MaxSumPt2");
      systematicTree->makeOutputVariable(miT_Tau_JpsiMass_MaxSumPt2,"Tau_JpsiMass_MaxSumPt2");

      systematicTree->makeOutputVariable(miT_Tau_InvMass_Min_A0,"Tau_InvMass_Min_A0");
      systematicTree->makeOutputVariable(miT_Tau_JpsiMass_Min_A0,"Tau_JpsiMass_Min_A0");

      systematicTree->makeOutputVariable(miT_Tau_InvMass_Min_Z0,"Tau_InvMass_Min_Z0");
      systematicTree->makeOutputVariable(miT_Tau_JpsiMass_Min_Z0,"Tau_JpsiMass_Min_Z0");
      
      systematicTree->makeOutputVariable(miT_TauErr_InvMass_MaxSumPt2,"TauErr_InvMass_MaxSumPt2");
      systematicTree->makeOutputVariable(miT_TauErr_JpsiMass_MaxSumPt2,"TauErr_JpsiMass_MaxSumPt2");
                                        
      systematicTree->makeOutputVariable(miT_TauErr_InvMass_Min_A0,"TauErr_InvMass_Min_A0");
      systematicTree->makeOutputVariable(miT_TauErr_JpsiMass_Min_A0,"TauErr_JpsiMass_Min_A0");

      systematicTree->makeOutputVariable(miT_TauErr_InvMass_Min_Z0,"TauErr_InvMass_Min_Z0");
      systematicTree->makeOutputVariable(miT_TauErr_JpsiMass_Min_Z0,"TauErr_JpsiMass_Min_Z0");

      systematicTree->makeOutputVariable(miT_Lxy_Min_A0,"Lxy_Min_A0");
      systematicTree->makeOutputVariable(miT_LxyErr_Min_A0,"LxyErr_Min_A0");

      systematicTree->makeOutputVariable(miT_Lxy_MaxSumPt2,"Lxy_MaxSumPt2");
      systematicTree->makeOutputVariable(miT_LxyErr_MaxSumPt2,"LxyErr_MaxSumPt2");

      systematicTree->makeOutputVariable(miT_Lxy_Min_Z0,"Lxy_Min_Z0");
      systematicTree->makeOutputVariable(miT_LxyErr_Min_Z0,"LxyErr_Min_Z0");

      systematicTree->makeOutputVariable(miT_Z0_Min_A0,"Z0_Min_A0");
      systematicTree->makeOutputVariable(miT_Z0Err_Min_A0,"Z0Err_Min_A0");

      systematicTree->makeOutputVariable(miT_Z0_Min_Z0,"Z0_Min_Z0");
      systematicTree->makeOutputVariable(miT_Z0Err_Min_Z0,"Z0Err_Min_Z0");

      systematicTree->makeOutputVariable(miT_Z0_MaxSumPt2,"Z0_MaxSumpt2");
      systematicTree->makeOutputVariable(miT_Z0Err_MaxSumPt2,"Z0Err_MaxSumPt2");

      systematicTree->makeOutputVariable(miT_A0_Min_A0,"A0_Min_A0");
      systematicTree->makeOutputVariable(miT_A0Err_Min_A0,"A0Err_Min_A0");

      systematicTree->makeOutputVariable(miT_A0_Min_Z0,"A0_Min_Z0");
      systematicTree->makeOutputVariable(miT_A0Err_Min_Z0,"A0Err_Min_Z0");

      systematicTree->makeOutputVariable(miT_A0_MaxSumPt2,"A0_MaxSumpt2");
      systematicTree->makeOutputVariable(miT_A0Err_MaxSumPt2,"A0Err_MaxSumPt2");

      systematicTree->makeOutputVariable(miT_A0xy_Min_A0,"A0xy_Min_A0");
      systematicTree->makeOutputVariable(miT_A0xyErr_Min_A0,"A0xyErr_Min_A0");
      
      systematicTree->makeOutputVariable(miT_A0xy_Min_Z0,"A0xy_Min_Z0");
      systematicTree->makeOutputVariable(miT_A0xyErr_Min_Z0,"A0xyErr_Min_Z0");

      systematicTree->makeOutputVariable(miT_A0xy_MaxSumPt2,"A0xy_MaxSumpt2");
      systematicTree->makeOutputVariable(miT_A0xyErr_MaxSumPt2,"A0xyErr_MaxSumPt2");

      systematicTree->makeOutputVariable(miT_pvx_orig_z_MaxSumPt2,"Pvx_orig_z");
      systematicTree->makeOutputVariable(miT_pvx_ref_z_MaxSumPt2,"Pvx_ref_z");

      systematicTree->makeOutputVariable(miT_isGood,"DiMuon_isGood");
      systematicTree->makeOutputVariable(miT_Muon_Author,"DiMuon_Authors");



      systematicTree->makeOutputVariable(m_Muon_Phi_hx,"Muon_Phi_hx");
      systematicTree->makeOutputVariable(m_Muon_CosTheta_hx,"Muon_CosTheta_hx");
      systematicTree->makeOutputVariable(m_Muon_Phi_hxOLD,"Muon_Phi_hxOLD");
      systematicTree->makeOutputVariable(m_Muon_CosTheta_hxOLD,"Muon_CosTheta_hxOLD");

      systematicTree->makeOutputVariable(m_Muon0_RecoEffTight,"Muon0_RecoEffTight");
      systematicTree->makeOutputVariable(m_Muon1_RecoEffTight,"Muon1_RecoEffTight");
      systematicTree->makeOutputVariable(m_Muon0_RecoEffMedium,"Muon0_RecoEffMedium");
      systematicTree->makeOutputVariable(m_Muon1_RecoEffMedium,"Muon1_RecoEffMedium");
      systematicTree->makeOutputVariable(m_Muon0_RecoEffLoose,"Muon0_RecoEffLoose");
      systematicTree->makeOutputVariable(m_Muon1_RecoEffLoose,"Muon1_RecoEffLoose");
      systematicTree->makeOutputVariable(m_Muon0_RecoEffLowPt,"Muon0_RecoEffLowPt");
      systematicTree->makeOutputVariable(m_Muon1_RecoEffLowPt,"Muon1_RecoEffLowPt");

      systematicTree->makeOutputVariable(m_Muon0_Quality,"Muon0_Quality");
      systematicTree->makeOutputVariable(m_Muon1_Quality,"Muon1_Quality");

      systematicTree->makeOutputVariable(m_Muon0_Charge,"Muon0_Charge");
      systematicTree->makeOutputVariable(m_Muon0_Track_d0,"Muon0_Track_d0");
      systematicTree->makeOutputVariable(m_Muon0_Track_z0,"Muon0_Track_z0");
      systematicTree->makeOutputVariable(m_Muon0_RefTrack_Pt,"Muon0_RefTrack_Pt");
      systematicTree->makeOutputVariable(m_Muon0_RefTrack_Eta,"Muon0_RefTrack_Eta");
      systematicTree->makeOutputVariable(m_Muon0_RefTrack_Phi,"Muon0_RefTrack_Phi");
      systematicTree->makeOutputVariable(m_Muon0_RefTrack_Theta,"Muon0_RefTrack_Theta");
      systematicTree->makeOutputVariable(m_Muon0_Track_Pt,"Muon0_Track_Pt");
      systematicTree->makeOutputVariable(m_Muon0_Track_Eta,"Muon0_Track_Eta");
      systematicTree->makeOutputVariable(m_Muon0_Track_Phi,"Muon0_Track_Phi");
      systematicTree->makeOutputVariable(m_Muon0_Track_Theta,"Muon0_Track_Theta");
      systematicTree->makeOutputVariable(m_Muon0_CBTrack_Pt,"Muon0_CBTrack_Pt");
      systematicTree->makeOutputVariable(m_Muon0_CBTrack_Eta,"Muon0_CBTrack_Eta");
      systematicTree->makeOutputVariable(m_Muon0_CBTrack_Phi,"Muon0_CBTrack_Phi");


      systematicTree->makeOutputVariable(m_Muon1_Charge,"Muon1_Charge");
      systematicTree->makeOutputVariable(m_Muon1_Track_d0,"Muon1_Track_d0");
      systematicTree->makeOutputVariable(m_Muon1_Track_z0,"Muon1_Track_z0");
      systematicTree->makeOutputVariable(m_Muon1_RefTrack_Pt,"Muon1_RefTrack_Pt");
      systematicTree->makeOutputVariable(m_Muon1_RefTrack_Eta,"Muon1_RefTrack_Eta");
      systematicTree->makeOutputVariable(m_Muon1_RefTrack_Phi,"Muon1_RefTrack_Phi");
      systematicTree->makeOutputVariable(m_Muon1_RefTrack_Theta,"Muon1_RefTrack_Theta");
      systematicTree->makeOutputVariable(m_Muon1_Track_Pt,"Muon1_Track_Pt");
      systematicTree->makeOutputVariable(m_Muon1_Track_Eta,"Muon1_Track_Eta");
      systematicTree->makeOutputVariable(m_Muon1_Track_Phi,"Muon1_Track_Phi");
      systematicTree->makeOutputVariable(m_Muon1_Track_Theta,"Muon1_Track_Theta");
      systematicTree->makeOutputVariable(m_Muon1_CBTrack_Pt,"Muon1_CBTrack_Pt");
      systematicTree->makeOutputVariable(m_Muon1_CBTrack_Eta,"Muon1_CBTrack_Eta");
      systematicTree->makeOutputVariable(m_Muon1_CBTrack_Phi,"Muon1_CBTrack_Phi");


      if(m_config->isMC()){
        systematicTree->makeOutputVariable(miT_TruthMatch,"TruthMatch");

        systematicTree->makeOutputVariable(miT_Truth_Jpsi_Pt,"Truth_Jpsi_Pt");
        systematicTree->makeOutputVariable(miT_Truth_Jpsi_Eta,"Truth_Jpsi_Eta");
        systematicTree->makeOutputVariable(miT_Truth_Jpsi_Phi,"Truth_Jpsi_Phi");
        systematicTree->makeOutputVariable(miT_Truth_Jpsi_Mass,"Truth_Jpsi_Mass");

        systematicTree->makeOutputVariable(miT_Truth_MuPlus_Pt,"Truth_MuPlus_Track_Pt");
        systematicTree->makeOutputVariable(miT_Truth_MuPlus_Eta,"Truth_MuPlus_Track_Eta");
        systematicTree->makeOutputVariable(miT_Truth_MuPlus_Phi,"Truth_MuPlus_Track_Phi");

        systematicTree->makeOutputVariable(miT_Truth_MuMinus_Pt,"Truth_MuMinus_Track_Pt");
        systematicTree->makeOutputVariable(miT_Truth_MuMinus_Eta,"Truth_MuMinus_Track_Eta");
        systematicTree->makeOutputVariable(miT_Truth_MuMinus_Phi,"Truth_MuMinus_Track_Phi");

        systematicTree->makeOutputVariable(miT_Truth_PV_Z,"Truth_PV_Z");
      }

/*      // vertex (check if it is not equal to m_vtxz)
      systematicTree->makeOutputVariable(m_vx,"vx");
      systematicTree->makeOutputVariable(m_vy,"vy");
      systematicTree->makeOutputVariable(m_vz,"vz");

      // original tracks  (should be equal to m_mu_jpsi_ ?)
      systematicTree->makeOutputVariable(m_trkOrigCharge1,"trkOrigCharge1");
      systematicTree->makeOutputVariable(m_trkOrigPx1,"trkOrigPx1");
      systematicTree->makeOutputVariable(m_trkOrigPy1,"trkOrigPy1");
      systematicTree->makeOutputVariable(m_trkOrigPz1,"trkOrigPz1");
      systematicTree->makeOutputVariable(m_trkOrigCharge2,"trkOrigCharge2");
      systematicTree->makeOutputVariable(m_trkOrigPx2,"trkOrigPx2");
      systematicTree->makeOutputVariable(m_trkOrigPy2,"trkOrigPy2");
      systematicTree->makeOutputVariable(m_trkOrigPz2,"trkOrigPz2");
*/      
    }

    //Added from BLSToolBox
    if (! m_muonSelectionTool_loose.setProperty("MuQuality",2).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_loose!");
    if (! m_muonSelectionTool_loose.setProperty("UseAllAuthors",true).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_loose!");
    if (! m_muonSelectionTool_loose.setProperty("MaxEta",2.5).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_loose!");
    if (! m_muonSelectionTool_loose.retrieve().isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_loose!");

    if (! m_muonSelectionTool_medium.setProperty("MuQuality",1).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_medium!");
    if (! m_muonSelectionTool_medium.setProperty("UseAllAuthors",true).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_medium!");
    if (! m_muonSelectionTool_medium.setProperty("MaxEta",2.5).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_medium!");
    if (! m_muonSelectionTool_medium.retrieve().isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_medium!");
    
    if (! m_muonSelectionTool_tight.setProperty("MuQuality",0).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_tight!");
    if (! m_muonSelectionTool_tight.setProperty("UseAllAuthors",true).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_tight!");
    if (! m_muonSelectionTool_tight.setProperty("MaxEta",2.5).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_tight!");
    if (! m_muonSelectionTool_tight.retrieve().isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_tight!");

    if (! m_muonSelectionTool_lowpt.setProperty("MuQuality",5).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_lowpt!");
    if (! m_muonSelectionTool_lowpt.setProperty("UseAllAuthors",true).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_lowpt!");
    if (! m_muonSelectionTool_lowpt.setProperty("MaxEta",2.5).isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_lowpt!");
    if (! m_muonSelectionTool_lowpt.retrieve().isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonSelectionTool_lowpt!");

    if (! m_muonCalibSmearTool.retrieve().isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonCalibSmearTool!");
    
    if (! m_muonEffSFTightTool.setProperty("WorkingPoint","Tight").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFTightTool!");
    if (! m_muonEffSFTightTool.setProperty("DataEfficiencyDecorationName","DefDataEfficiencyTight").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFTightTool!");
    if (! m_muonEffSFTightTool.setProperty("MCEfficiencyDecorationName"  ,"DefMCEfficiencyTight").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFTightTool!");
    if (! m_muonEffSFTightTool.setProperty("ScaleFactorDecorationName"   ,"DefEfficiencyScaleFactorTight").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFTightTool!");
    if (! m_muonEffSFTightTool.retrieve().isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFTightTool!");

    if (! m_muonEffSFMediumTool.setProperty("WorkingPoint","Medium").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFMediumTool!");
    if (! m_muonEffSFMediumTool.setProperty("DataEfficiencyDecorationName","DefDataEfficiencyMedium").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFMediumTool!");
    if (! m_muonEffSFMediumTool.setProperty("MCEfficiencyDecorationName"  ,"DefMCEfficiencyMedium").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFMediumTool!");
    if (! m_muonEffSFMediumTool.setProperty("ScaleFactorDecorationName"   ,"DefEfficiencyScaleFactorMedium").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFMediumTool!");
    if (! m_muonEffSFMediumTool.retrieve().isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFMediumTool!");

    if (! m_muonEffSFLooseTool .setProperty("WorkingPoint","Loose").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLooseTool!");
    if (! m_muonEffSFLooseTool.setProperty("DataEfficiencyDecorationName","DefDataEfficiencyLoose").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLooseTool!");
    if (! m_muonEffSFLooseTool.setProperty("MCEfficiencyDecorationName"  ,"DefMCEfficiencyLoose").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLooseTool!");
    if (! m_muonEffSFLooseTool.setProperty("ScaleFactorDecorationName"   ,"DefEfficiencyScaleFactorLoose").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLooseTool!");
    if (! m_muonEffSFLooseTool.retrieve().isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLooseTool!");

    if (! m_muonEffSFLowPtTool.setProperty("WorkingPoint","LowPt").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLowPtTool!");
    if (! m_muonEffSFLowPtTool.setProperty("DataEfficiencyDecorationName","DefDataEfficiencyLowPt").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLowPtTool!");
    if (! m_muonEffSFLowPtTool.setProperty("MCEfficiencyDecorationName"  ,"DefMCEfficiencyLowPt").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLowPtTool!");
    if (! m_muonEffSFLowPtTool.setProperty("ScaleFactorDecorationName"   ,"DefEfficiencyScaleFactorLowPt").isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLowPtTool!");
    if (! m_muonEffSFLowPtTool.retrieve().isSuccess() ) Warning("TtbarJpsiEventSaver::initialize()","Problem with m_muonEffSFLowPtTool!");



/*   if (m_muonEffSFLowPtTool.retrieve().isFailure()) {
        ATH_MSG_ERROR("LowPt Muon WP reco Eff not yet available!");
    }

*/

/*
    //MCP initialisation for JPsi muons, maybe we need better alternative
    m_muonSelection = new CP::MuonSelectionTool("MuonSelection");

    //m_muonSelection->msg().setLevel( MSG::INFO );
    if(!m_muonSelection->setProperty("MaxEta",2.5).isSuccess()) Warning("JpsiAnalysis::Setup()","Problem with MuonSelectionTool!");
    // Medium
    if(!m_muonSelection->setProperty("MuQuality",1).isSuccess()) Warning("JpsiAnalysis::Setup()","Problem with MuonSelectionTool!");
    
    if(!m_muonSelection->initialize().isSuccess()) {
        Info("JpsiAnalysis::Setup()", "Problem in Initialize MuonSelectionTool!");
        return ;
    }


    m_muonCalibTool = new CP::MuonCalibrationAndSmearingTool("MuonCalibTool");
    if(!m_muonCalibTool->setProperty("Year", "Data16").isSuccess()) //Data16 or Data15
      Warning("JpsiAnalysis::Setup()","Problem with MuonCalibrationAndSmearingTool!");
//    if(!m_muonCalibTool->setProperty("Release", "Recs2016_15_07").isSuccess()) Warning("JpsiAnalysis::Setup()","Problem with MuonCalibrationAndSmearingTool!");
    if(!m_muonCalibTool->setProperty("StatComb", true).isSuccess()) Warning("JpsiAnalysis::Setup()","Problem with MuonCalibrationAndSmearingTool!");
    if(!m_muonCalibTool->initialize().isSuccess()) {
        Info("JpsiAnalysis::Setup()", "Problem in Initialize MuonCalibrationAndSmearingTool!");
        return ;
    }
    // MuonEfficiencyScaleFactors
    m_muonEfficiency = new CP::MuonEfficiencyScaleFactors("MuonRecoScaleFactor");
    m_muonEfficiency->msg().setLevel(MSG::ERROR);
    if(!m_muonEfficiency->setProperty("WorkingPoint","Medium").isSuccess()) Warning("JpsiAnalysis::Setup()","Problem with MuonEfficiencyScaleFactors!");
//    if(!m_muonEfficiency->setProperty("CalibrationRelease","170303_Moriond")) Warning("JpsiAnalysis::Setup()","Problem with MuonEfficiencyScaleFactors!");
    if(!m_muonEfficiency->initialize().isSuccess()) {
        Info("JpsiAnalysis::Setup()", "Problem in Initialize MuonEfficiencyScaleFactors");
        return ;
    }
*/
  } 

  // ======================================================================  
  void TtbarJpsiEventSaver::saveEvent(const top::Event& event) 
  {
    //
    // saveEvent - run for every systematic and every event
    // 
    
    // record the event?
    if (m_config->saveOnlySelectedEvents() && !event.m_saveEvent)
      return;

    // Filling up variables which are not already provided 
    // by the TopAnalysis event saver

    // add vertex information
    AddVertex(event);
    
    // add electrons from top
    AddElectrons(event);

    // add muons from top
    AddMuons(event);

    // add jets from top
    AddJets(event); 

    // add MET
    AddMET(event);

    // add other muons to form the Jpsi candidates
    AddMuonsForJpsi(event);
    
    // add Jpsi candidates from BPHY1 part
    AddJpsi(event);
    if(m_config->isMC())  AddJpsiTruth(event);
    // for trigger prescales
    AddTriggerPrescales(); 

    
    top::EventSaverFlatNtuple::saveEvent(event);
  }
  
 // ==================================================================
  void TtbarJpsiEventSaver::AddVertex(const top::Event& event) 
  {    
    //
    // keep information from primary vertex
    //

    // event info
    if (!m_config->isMC()) m_lumiBlock   = event.m_info->lumiBlock();
    m_npvtx = 0;
    m_vtxz  = 0;

    const xAOD::VertexContainer *m_primvtx = event.m_primaryVertices;
    for (const auto* const vtxPtr : *m_primvtx) {
      const xAOD::VxType::VertexType vtype = vtxPtr->vertexType();
      const int vmult = vtxPtr->trackParticleLinks().size();

      // count vertices of type 1 (primary) and 3 (pileup) with >4 tracks
      if ((vtype==1 || vtype==3) && vmult>4) {
	++m_npvtx;
	// assuming there is only one primary vertex
	if (vtype==1) {
	  m_vtxz = vtxPtr->z();
	}
      }
    }

    return;
  }
 
  // ==================================================================
  void TtbarJpsiEventSaver::AddElectrons(const top::Event& event) 
  {    
    //
    // keep information for standard electrons (i.e the ones from W)
    //

    if (!m_config->useElectrons()) return;

    // number of electrons (using standard container event.m_electrons)
    m_el_n = event.m_electrons.size();
    // trigger matching
    m_el_trigMatch.resize(m_el_n);
    
    // loop on electrons
    unsigned int iel(0);
    for (const auto* const elPtr : event.m_electrons) {
      // get trigmatch variables
      m_el_trigMatch[iel] = 0;
      unsigned int runNumber = event.m_info -> runNumber();
      if (m_config->isMC() && m_config->doPileupReweighting()) {
	if (event.m_info->isAvailable<unsigned int>("RandomRunNumber"))
	  runNumber = event.m_info->auxdataConst<unsigned int>("RandomRunNumber");
      }
      
//      top::ttjpsi::GetTrigMatchVariable(*elPtr,m_el_trigMatch[iel],runNumber);
      
      iel++;
    } // loop on electrons
    
    return;
  }

  // ==================================================================
  void TtbarJpsiEventSaver::AddMuons(const top::Event& event) 
  {    
    //
    // keep information for standard muons (i.e the ones from W)
    //

    if (!m_config->useMuons()) return;

    // number of muons from standard container event.m_muons
    m_mu_n = event.m_muons.size();      
    // resize containers
    m_mu_trigMatch.resize(m_mu_n);
    m_mu_d0.resize(m_mu_n);
    m_mu_z0.resize(m_mu_n);
    //std::cout << " n muons = " << m_mu_n << std::endl;
    
    // loop on muons 
    unsigned int imu(0);
    for (const auto* const muPtr : event.m_muons) {
      const xAOD::TrackParticle* mutrack = muPtr->primaryTrackParticle();
      if (mutrack!=nullptr) {
        m_mu_d0[imu] = mutrack->d0();
        m_mu_z0[imu] = mutrack->z0()+mutrack->vz() - m_vtxz;
      }
      // trigger matching
      m_mu_trigMatch[imu] = 0;
      // retrieve the run number
      unsigned int runNumber = event.m_info -> runNumber();
      if (m_config->isMC() && m_config->doPileupReweighting()) {
	if (event.m_info->isAvailable<unsigned int>("RandomRunNumber"))
	  runNumber = event.m_info->auxdataConst<unsigned int>("RandomRunNumber");
      }
//      top::ttjpsi::GetTrigMatchVariable(*muPtr,m_mu_trigMatch[imu],runNumber);
      
      imu++;
    } //loop on muons
 
    return;
  }

  // ==================================================================
  void TtbarJpsiEventSaver::AddJets(const top::Event& event) 
  {    
    //
    // keep information for all jets
    //

    if (!m_config->useJets()) return;

    // number of jets
    m_jet_n = 0;      
    //resize containers
    m_jet_m.resize(event.m_jets.size());
    
    // loop on jets (using standard container event.m_jets)
    unsigned int ijet(0);
    for (const auto* const jetPtr : event.m_jets) {
      m_jet_n++;
      //jet mass
      TLorentzVector jet;
      jet.SetPtEtaPhiE(jetPtr->pt(),jetPtr->eta(),jetPtr->phi(),jetPtr->e());
      m_jet_m[ijet] = jetPtr->m();	
      ijet++;
    }
 
    return;
  }

  // ==================================================================
  void TtbarJpsiEventSaver::AddMET(const top::Event& event) 
  {    
    //
    // keep information for MET
    //
    
    if (!m_config->useJets()) return;

    m_met_px    = event.m_met->mpx();
    m_met_py    = event.m_met->mpy();
    m_met_sumet = event.m_met->sumet();

    return;
  }

  // ==================================================================
  void TtbarJpsiEventSaver::AddMuonsForJpsi(const top::Event& event) 
  {    
    //
    // keep information for all muons (to get the ones to form the J/psi)
    //
    
    if (!m_config->useMuons()) return;
    
    // retrieve all muons form original full container
    const xAOD::MuonContainer* calibratedMuons(nullptr);
    top::check(evtStore()->retrieve(calibratedMuons, 
				    m_config->sgKeyMuons(event.m_hashValue) ), 
	       "Failed to retrieve muons");
    
    // number of muons
    m_mu_jpsi_n = calibratedMuons->size();      
    //std::cout << " n muons for J/psi = " << m_mu_jpsi_n << std::endl;
    // resize containers
    m_mu_jpsi_author.resize(m_mu_jpsi_n);
    m_mu_jpsi_pt.resize(m_mu_jpsi_n);
    m_mu_jpsi_eta.resize(m_mu_jpsi_n);
    m_mu_jpsi_phi.resize(m_mu_jpsi_n);
    m_mu_jpsi_charge.resize(m_mu_jpsi_n);
    m_mu_jpsi_d0.resize(m_mu_jpsi_n);
    m_mu_jpsi_z0.resize(m_mu_jpsi_n);
    m_mu_jpsi_d0sig.resize(m_mu_jpsi_n);
    m_mu_jpsi_delta_z0_sintheta.resize(m_mu_jpsi_n);
    // for overlap removal
    m_mu_jpsi_overlap.resize(m_mu_jpsi_n);
    // for pid
    m_mu_jpsi_Tight.resize(m_mu_jpsi_n);
    m_mu_jpsi_Medium.resize(m_mu_jpsi_n);
    m_mu_jpsi_Loose.resize(m_mu_jpsi_n);
    m_mu_jpsi_VeryLoose.resize(m_mu_jpsi_n);
    // for isolation
    m_mu_jpsi_topoetcone20.resize(m_mu_jpsi_n);
    m_mu_jpsi_topoetcone30.resize(m_mu_jpsi_n);
    m_mu_jpsi_topoetcone40.resize(m_mu_jpsi_n);
    m_mu_jpsi_ptvarcone20.resize(m_mu_jpsi_n);
    m_mu_jpsi_ptvarcone30.resize(m_mu_jpsi_n);
    m_mu_jpsi_ptvarcone40.resize(m_mu_jpsi_n);
    // truth level
    if(m_config->isMC()) {
      m_mu_jpsi_true_type.resize(m_mu_jpsi_n);
      m_mu_jpsi_true_origin.resize(m_mu_jpsi_n);
    }
 
   
    // loop on additional muons
    unsigned int imu_jpsi(0);
    for (const auto* const muJpsiPtr : *calibratedMuons) {
      // author of the algorithm
      m_mu_jpsi_author[imu_jpsi] = muJpsiPtr->author();
      // pt
      m_mu_jpsi_pt[imu_jpsi] = muJpsiPtr->pt();
      // eta
      m_mu_jpsi_eta[imu_jpsi] = muJpsiPtr->eta();
      // phi
      m_mu_jpsi_phi[imu_jpsi] = muJpsiPtr->phi();
      // charge
      m_mu_jpsi_charge[imu_jpsi] = muJpsiPtr->charge();
      // d0
      m_mu_jpsi_d0[imu_jpsi] = -9999.;
      // z0
      m_mu_jpsi_z0[imu_jpsi] = -9999.;
      const xAOD::TrackParticle* mutrack = muJpsiPtr->primaryTrackParticle();
      if (mutrack!=nullptr) {
	m_mu_jpsi_d0[imu_jpsi] = mutrack->d0();
	m_mu_jpsi_z0[imu_jpsi] = mutrack->z0()+mutrack->vz() - m_vtxz;
      }
      // d0sig
      m_mu_jpsi_d0sig[imu_jpsi] = -9999.;
      if( muJpsiPtr->isAvailable<float>("d0sig") ){
	m_mu_jpsi_d0sig[imu_jpsi] = muJpsiPtr->auxdataConst<float>("d0sig");
      }
      // delta_z0_sintheta
      m_mu_jpsi_delta_z0_sintheta[imu_jpsi] = -9999.;
      if( muJpsiPtr->isAvailable<float>("delta_z0_sintheta") ){
	m_mu_jpsi_delta_z0_sintheta[imu_jpsi] = muJpsiPtr->auxdataConst<float>("delta_z0_sintheta");
      }
      
      // overlap
      auto x = muJpsiPtr->auxdecor<char>("overlaps");
      m_mu_jpsi_overlap[imu_jpsi] = x;
/*      
      // retrieve the quality variables
      xAOD::Muon::Quality quality = m_muonsel->getQuality(*muJpsiPtr);
      m_mu_jpsi_Tight[imu_jpsi]     = quality <= xAOD::Muon::Tight ? true : false;
      m_mu_jpsi_Medium[imu_jpsi]    = quality <= xAOD::Muon::Medium ? true : false;
      m_mu_jpsi_Loose[imu_jpsi]     = quality <= xAOD::Muon::Loose ? true : false;
      m_mu_jpsi_VeryLoose[imu_jpsi] = quality <= xAOD::Muon::VeryLoose ? true : false;
*/      
      //std::cout << " decor = " << x << " pt " << muJpsiPtr->pt() << " Tight = " << m_mu_jpsi_Tight[imu_jpsi] << std::endl;
      
      // adding other isolation variables
      muJpsiPtr->isolation(m_mu_jpsi_topoetcone20[imu_jpsi], 
			   xAOD::Iso::topoetcone20 );
      muJpsiPtr->isolation(m_mu_jpsi_topoetcone30[imu_jpsi], 
			   xAOD::Iso::topoetcone30 );
      muJpsiPtr->isolation(m_mu_jpsi_topoetcone40[imu_jpsi],
			   xAOD::Iso::topoetcone40 );
      muJpsiPtr->isolation(m_mu_jpsi_ptvarcone20[imu_jpsi],
			   xAOD::Iso::ptvarcone20 );
      muJpsiPtr->isolation(m_mu_jpsi_ptvarcone30[imu_jpsi],
			   xAOD::Iso::ptvarcone30 );
      muJpsiPtr->isolation(m_mu_jpsi_ptvarcone40[imu_jpsi],
			   xAOD::Iso::ptvarcone40 );
      
      // truth matching
      if(m_config->isMC()) {
	static SG::AuxElement::Accessor<int> acc_mctt("truthType");
	static SG::AuxElement::Accessor<int> acc_mcto("truthOrigin");
	m_mu_jpsi_true_type[imu_jpsi]   = 0;
	m_mu_jpsi_true_origin[imu_jpsi] = 0;
	const xAOD::TrackParticle* mutrack = muJpsiPtr->primaryTrackParticle();
	if (mutrack!=nullptr) {
	  if (acc_mctt.isAvailable(*mutrack)) m_mu_jpsi_true_type[imu_jpsi] = acc_mctt(*mutrack);
	  if (acc_mcto.isAvailable(*mutrack)) m_mu_jpsi_true_origin[imu_jpsi] = acc_mcto(*mutrack);
	}
      }
      
      imu_jpsi++;
    }

    return;
  }

  // ==================================================================
  void TtbarJpsiEventSaver::AddJpsi(const top::Event& event) 
  {    
    //Clean vectors//
    miT_PV_MaxSumPt2_Z.clear();
    miT_PV_Min_A0_Z.clear();
    miT_PV_Min_Z0_Z.clear();
    miT_DiMuon_Mass.clear();
    miT_DiMuon_Mass_Err.clear();
    miT_DiMuon_Vtxx.clear();
    miT_DiMuon_Vtxy.clear();
    miT_DiMuon_Vtxz.clear();
    miT_DiMuon_Pt.clear();
    miT_DiMuon_Rapidity.clear();
    miT_DiMuon_Eta.clear();
    miT_DiMuon_Phi.clear();
    miT_MuPlus_Track_Pt.clear();
    miT_MuPlus_Track_Eta.clear();
    miT_MuPlus_Track_Phi.clear();
    miT_MuPlus_Tight.clear();
    miT_MuPlus_RecoEff.clear();
    miT_MuMinus_Track_Pt.clear();
    miT_MuMinus_Track_Eta.clear();
    miT_MuMinus_Track_Phi.clear();
    miT_MuMinus_Tight.clear();
    miT_MuMinus_RecoEff.clear();
    miT_Tau_InvMass_MaxSumPt2.clear();
    miT_Tau_JpsiMass_MaxSumPt2.clear();
    miT_Tau_InvMass_Min_A0.clear();
    miT_Tau_JpsiMass_Min_A0.clear();
    miT_Tau_InvMass_Min_Z0.clear();
    miT_Tau_JpsiMass_Min_Z0.clear();
    miT_TauErr_InvMass_MaxSumPt2.clear();
    miT_TauErr_JpsiMass_MaxSumPt2.clear();
    miT_TauErr_InvMass_Min_A0.clear();
    miT_TauErr_JpsiMass_Min_A0.clear();
    miT_TauErr_InvMass_Min_Z0.clear();
    miT_TauErr_JpsiMass_Min_Z0.clear();
    miT_Lxy_Min_A0.clear();
    miT_LxyErr_Min_A0.clear();
    miT_Lxy_MaxSumPt2.clear();
    miT_LxyErr_MaxSumPt2.clear();
    miT_Lxy_Min_Z0.clear();
    miT_LxyErr_Min_Z0.clear();
    miT_Z0_Min_A0.clear();
    miT_Z0Err_Min_A0.clear();
    miT_Z0_Min_Z0.clear();
    miT_Z0Err_Min_Z0.clear();
    miT_Z0_MaxSumPt2.clear();
    miT_Z0Err_MaxSumPt2.clear();
    miT_A0_Min_A0.clear();
    miT_A0Err_Min_A0.clear();
    miT_A0_Min_Z0.clear();
    miT_A0Err_Min_Z0.clear();
    miT_A0_MaxSumPt2.clear();
    miT_A0Err_MaxSumPt2.clear();
    miT_A0xy_Min_A0.clear();
    miT_A0xyErr_Min_A0.clear();
    miT_A0xy_Min_Z0.clear();
    miT_A0xyErr_Min_Z0.clear();
    miT_A0xy_MaxSumPt2.clear();
    miT_A0xyErr_MaxSumPt2.clear();
    miT_pvx_orig_z_MaxSumPt2.clear();
    miT_pvx_ref_z_MaxSumPt2.clear();


    miT_TruthMatch.clear();
    miT_isGood.clear();
    miT_Muon_Author.clear();

    // Angles
    m_Muon_Phi_hx.clear();              //!
    m_Muon_CosTheta_hx.clear();         //!
    m_Muon_Phi_hxOLD.clear();              //!
    m_Muon_CosTheta_hxOLD.clear();        //!

    // Reco Eff. flags
    m_Muon0_RecoEffTight.clear();     //!
    m_Muon1_RecoEffTight.clear();     //!
    m_Muon0_RecoEffMedium.clear();       //!
    m_Muon1_RecoEffMedium.clear();       //!
    m_Muon0_RecoEffLoose.clear();      //!
    m_Muon1_RecoEffLoose.clear();       //!
    m_Muon0_RecoEffLowPt.clear();       //!
    m_Muon1_RecoEffLowPt.clear();       //!

    //muon quality flags
    m_Muon0_Quality.clear();            //!
    m_Muon1_Quality.clear();            //!

    m_Muon0_RefTrack_Pt.clear();
    m_Muon0_RefTrack_Eta.clear();
    m_Muon0_RefTrack_Phi.clear();
    m_Muon0_RefTrack_Theta.clear();
    m_Muon0_Track_Pt.clear();
    m_Muon0_Track_Eta.clear();
    m_Muon0_Track_Phi.clear();
    m_Muon0_Track_Theta.clear();
    m_Muon0_Track_d0.clear();
    m_Muon0_Track_z0.clear();
    m_Muon0_CBTrack_Pt.clear();
    m_Muon0_CBTrack_Eta.clear();
    m_Muon0_CBTrack_Phi.clear();
    m_Muon0_Charge.clear();
    m_Muon0_Quality.clear();

    m_Muon1_RefTrack_Pt.clear();
    m_Muon1_RefTrack_Eta.clear();
    m_Muon1_RefTrack_Phi.clear();
    m_Muon1_RefTrack_Theta.clear();
    m_Muon1_Track_Pt.clear();
    m_Muon1_Track_Eta.clear();
    m_Muon1_Track_Phi.clear();
    m_Muon1_Track_Theta.clear();
    m_Muon1_Track_d0.clear();
    m_Muon1_Track_z0.clear();
    m_Muon1_CBTrack_Pt.clear();
    m_Muon1_CBTrack_Eta.clear();
    m_Muon1_CBTrack_Phi.clear();
    m_Muon1_Charge.clear();
    m_Muon1_Quality.clear();



    //------------//
    const xAOD::VertexContainer* DiMuonVertices = 0;
    top::check( evtStore()->retrieve(DiMuonVertices, "TOPQ5OniaCandidates" ), "Failed to retrieve TOPQ5OniaCandidates container. Exiting.");
    for(unsigned int j = 0; j < DiMuonVertices->size(); ++j){
      const xAOD::Vertex * DiMuon = (*DiMuonVertices)[j];
      
      double DiMuon_Mass = DiMuon->auxdata<float>("Jpsi_mass");
      double DiMuon_MassErr = DiMuon->auxdata<float>("Jpsi_massErr");



      // Cut away Upsilon for now...
      //if(DiMuon_Mass < 2.0e3 || DiMuon_Mass > 4.5e3) continue;

      // Helper Tool
      xAOD::BPhysHelper bVtx(DiMuon);
 
      std::vector< ElementLink<xAOD::MuonContainer> > MuonLinks = DiMuon->auxdata< std::vector< ElementLink<xAOD::MuonContainer> > >("MuonLinks");
 

      const ElementLink< xAOD::MuonContainer >& MuonLink0 = MuonLinks[0];
      const ElementLink< xAOD::MuonContainer >& MuonLink1 = MuonLinks[1];

      const xAOD::Muon * Mu0 = NULL;
      const xAOD::Muon * Mu1 = NULL;


      if (MuonLink0.isValid()) Mu0 = *MuonLink0;
      if (MuonLink1.isValid()) Mu1 = *MuonLink1;
      if (Mu0 == NULL || Mu1 == NULL) continue;

      auto Muon0_ptr = makeCalibMuon(Mu0);
      auto Muon1_ptr = makeCalibMuon(Mu1);

        // set the quality of the muons:
        // logic is Tight=0, Medium=1, Loose=2, LowPT=5;
        // stored as 1*isTight + 10*isMedium + 100*isLoose + 1000*LowPt
        int muon0_qual(0);
        int muon1_qual(0);
        bool muon0_loose(false), muon0_medium(false),muon0_tight(false), muon0_lowpt(false);
        bool muon1_loose(false), muon1_medium(false),muon1_tight(false), muon1_lowpt(false);
        bool bvar(false);
        if (passMuonTight (Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 1;   muon0_tight=true;}
        if (passMuonMedium(Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 10;  muon0_medium=true;}
        if (passMuonLoose (Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 100; muon0_loose=true;}
        if (passMuonLowPt (Muon0_ptr.get(),bvar) && bvar) {muon0_qual += 1000;muon0_lowpt=true;}
        
        if (passMuonTight (Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 1;   muon1_tight=true;}
        if (passMuonMedium(Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 10;  muon1_medium=true;}
        if (passMuonLoose (Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 100; muon1_loose=true;}
        if (passMuonLowPt (Muon1_ptr.get(),bvar) && bvar) {muon1_qual += 1000;muon1_lowpt=true;}

        // un comment this section; assume that selection of muon quality, will be done in ntuple
        //        if (!passMuonTight(Muon0_ptr.get(),bvar) || !bvar) continue;
        //        if (!passMuonTight(Muon1_ptr.get(),bvar) || !bvar) continue;


      /*
      bool Mu1 = 0, Mu2 =0;
      int index(0), auth1(0), auth2(0);
      for (const auto* const muPtr : event.m_muons) {
        if( Muon0->p4() == muPtr->p4() ){ Mu1 = 1; auth1 = index; }
        if( Muon1->p4() == muPtr->p4() ){ Mu2 = 1; auth2 = index; }
        index++;
      }
      
      //::: Create a calibrated muon:
      xAOD::Muon* Muon0 = NULL;
      xAOD::Muon* Muon1 = NULL;




      if (!m_muonCalibTool->correctedCopy(*Mu0, Muon0) || !m_muonCalibTool->correctedCopy(*Mu1, Muon1) ) {
        if (Muon0) { delete Muon0; Muon0 = NULL; }
        if (Muon1) { delete Muon1; Muon1 = NULL; }
        continue;
      }

      //===============================================================
      // Apply MCP selection
      //===============================================================
      if (!m_muonSelection->accept(*Muon0) || !m_muonSelection->accept(*Muon1)) {
        if (Muon0) { delete Muon0; Muon0 = NULL; }
        if (Muon1) { delete Muon1; Muon1 = NULL; }
        continue;
      }
      //===============================================================

      xAOD::Muon::Quality mu_quality;
      bool mu0_tight(0), mu1_tight(0);

      mu_quality = m_muonSelection->getQuality(*Muon0);
      if(mu_quality==xAOD::Muon::Tight) mu0_tight=1;
      mu_quality = m_muonSelection->getQuality(*Muon1);
      if(mu_quality==xAOD::Muon::Tight) mu1_tight=1;
*/
      bool mu0_tight(0), mu1_tight(0);

      const xAOD::TrackParticle * Track0 = NULL;
      const xAOD::TrackParticle * Track1 = NULL;
 
      const ElementLink< xAOD::TrackParticleContainer >& Link0 = Muon0_ptr->inDetTrackParticleLink();
      const ElementLink< xAOD::TrackParticleContainer >& Link1 = Muon1_ptr->inDetTrackParticleLink();

      if(Link0.isValid()) Track0 = *Link0;
      if(Link1.isValid()) Track1 = *Link1;
 
      if(Track0 == NULL || Track1 == NULL) continue;
/*
      //===============================================================
      // Apply MCP track selection - not really needed, should be applied above
      //===============================================================
      if(!m_muonSelection->passedIDCuts(*Track0)) continue;
      if(!m_muonSelection->passedIDCuts(*Track1)) continue;
      //===============================================================

      //===============================================================
      // Require both combined muons...
      //===============================================================
      if(Muon0->muonType() != xAOD::Muon::Combined) continue;
      if(Muon1->muonType() != xAOD::Muon::Combined) continue;
      //===============================================================
*/
      TLorentzVector Muon0_RefTrack = bVtx.refTrk(0,m_MuonMass);
      TLorentzVector Muon1_RefTrack = bVtx.refTrk(1,m_MuonMass);
 
      TLorentzVector RefDiMuon = Muon0_RefTrack + Muon1_RefTrack;


      //==============================================================
      // Tag bad J/Psi
      //==============================================================
      if( event.m_muons.size()>0  && (Muon0_ptr->p4() == event.m_muons.at(0)->p4() || Muon1_ptr->p4() == event.m_muons.at(0)->p4()) ) miT_isGood.push_back(0);
      else miT_isGood.push_back(1);

        // requires that muons have pass the Tight selection (as should be applied above).
        
        float _recoEffMuon0 = 1;
        float _recoEffMuon1 = 1;
        auto eventInfo    = eventInfo_f();

        // Muon 0
        _recoEffMuon0 = -1.;
        if (muon0_tight) {
            auto muonEffSFTool = muonEffSFTightTool();
            auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
            if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
                ATH_MSG_INFO ("corrCodeMu0Eff Tight " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " <<Muon0_ptr->phi());
            }
        }
        m_Muon0_RecoEffTight.push_back( _recoEffMuon0);

        _recoEffMuon0 = -1.;
        if (muon0_medium) {
            auto muonEffSFTool = muonEffSFMediumTool();
            auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
            if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
                ATH_MSG_INFO ("corrCodeMu0Eff Medium " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " << Muon0_ptr->phi());
            }
        }
        m_Muon0_RecoEffMedium.push_back( _recoEffMuon0);
        _recoEffMuon0 = -1.;
        if (muon0_loose) {
            auto muonEffSFTool = muonEffSFLooseTool();
            auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
            if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
                ATH_MSG_INFO ("corrCodeMu0Eff Loose " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " << Muon0_ptr->phi());
            }
        }
        m_Muon0_RecoEffLoose.push_back( _recoEffMuon0);
        _recoEffMuon0 = -1.;
        if (muon0_lowpt && muonEffSFLowPtTool()) {
            auto muonEffSFTool = muonEffSFLowPtTool();
            auto corrCodeMu0Eff = muonEffSFTool->getDataEfficiency(*Muon0_ptr, _recoEffMuon0, eventInfo);
            if (corrCodeMu0Eff != CP::CorrectionCode::Ok) {
                ATH_MSG_INFO ("corrCodeMu0Eff LowPt " << corrCodeMu0Eff << " " << Muon0_ptr->pt() << " " << Muon0_ptr->eta() << " " << Muon0_ptr->phi());
            }
        }
        m_Muon0_RecoEffLowPt.push_back( _recoEffMuon0);

        // Muon 1
        _recoEffMuon1 = -1.;
        if (muon1_tight) {
            auto muonEffSFTool = muonEffSFTightTool();
            auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
            if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
                ATH_MSG_INFO ("corrCodeMu1Eff Tight " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
            }
        }
        m_Muon1_RecoEffTight.push_back( _recoEffMuon1);
        
        _recoEffMuon1 = -1.;
        if (muon1_medium) {
            auto muonEffSFTool = muonEffSFMediumTool();
            auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
            if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
                ATH_MSG_INFO ("corrCodeMu1Eff Medium " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
            }
        }
        m_Muon1_RecoEffMedium.push_back( _recoEffMuon1);
        _recoEffMuon1 = -1.;
        if (muon1_loose) {
            auto muonEffSFTool = muonEffSFLooseTool();
            auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
            if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
                ATH_MSG_INFO ("corrCodeMu1Eff Loose " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
            }
        }
        m_Muon1_RecoEffLoose.push_back( _recoEffMuon1);
        _recoEffMuon1 = -1.;
        if (muon1_lowpt && muonEffSFLowPtTool()) {
            auto muonEffSFTool = muonEffSFLowPtTool();
            auto corrCodeMu1Eff = muonEffSFTool->getDataEfficiency(*Muon1_ptr, _recoEffMuon1, eventInfo);
            if (corrCodeMu1Eff != CP::CorrectionCode::Ok) {
                ATH_MSG_INFO ("corrCodeMu1Eff LowPt " << corrCodeMu1Eff << " " << Muon1_ptr->pt() << " " << Muon1_ptr->eta() << " " << Muon1_ptr->phi());
            }
        }
        m_Muon1_RecoEffLowPt.push_back( _recoEffMuon1);

        
        
        
        double phi_hxOLD      = phiMethod(Muon0_RefTrack,Muon1_RefTrack,Muon0_ptr->charge());
        double cosTheta_hxOLD = cosMethod(Muon0_RefTrack,Muon1_RefTrack,Muon0_ptr->charge());
        
//        m_muonEfficiency->getDataEfficiency(*Muon0, _recoEffMuon0, eventInfo);
//        m_muonEfficiency->getDataEfficiency(*Muon1, _recoEffMuon1, eventInfo);
//        //Info("execute()", "Muon0 pT = %.2f, Muon0 eta = %.2f, Efficiency = %.3f", Muon0->pt()/1e3, Muon0->eta(), _recoEffMuon0 );
//        //Info("execute()", "Muon1 pT = %.2f, Muon1 eta = %.2f, Efficiency = %.3f", Muon1->pt()/1e3, Muon1->eta(), _recoEffMuon1 );
        
        m_Muon_Phi_hxOLD     .push_back( phi_hxOLD);
        m_Muon_CosTheta_hxOLD.push_back( cosTheta_hxOLD);
        double phi_hx(0.);
        double cosTheta_hx(0.);
        if (Muon0_ptr->charge() > 0) {
            VatoHelicity(Muon0_RefTrack,Muon1_RefTrack,
                                    cosTheta_hx,phi_hx);
        } else {
            VatoHelicity(Muon1_RefTrack,Muon0_RefTrack,
                                    cosTheta_hx,phi_hx);
        }


        
        m_Muon_Phi_hx     .push_back( phi_hx);
        m_Muon_CosTheta_hx.push_back( cosTheta_hx);

/*
      //==============================================================
      // Muon Efficiency
      //==============================================================
      float mu0_recoEff = -1;
      float mu1_recoEff = -1;

      top::check(m_muonEfficiency->getDataEfficiency(*Muon0, mu0_recoEff, event.m_info),"Failed to get SF for Muon0");
      top::check(m_muonEfficiency->getDataEfficiency(*Muon1, mu1_recoEff, event.m_info),"Failed to get SF for Muon0");
*/

/*
      if( !Mu1 || !Mu2 ) miT_isGood.push_back(0);
      else {
        miT_isGood.push_back(1);
        miT_Muon_Author.push_back(auth1);
        miT_Muon_Author.push_back(auth2);
      }
*/
      //===============================================================
      // Vertex
      //===============================================================
/*      const xAOD::Vertex* RefPV_Min_A0 = bVtx.pv(xAOD::BPhysHelper::PV_MIN_A0);
      const xAOD::Vertex* RefPV_Min_Z0 = bVtx.pv(xAOD::BPhysHelper::PV_MIN_Z0);
      const xAOD::Vertex* RefPV_Max_Sum_Pt2 = bVtx.pv(xAOD::BPhysHelper::PV_MAX_SUM_PT2);
      std::cout << RefPV_Min_A0 << " " << RefPV_Min_Z0 << " " << RefPV_Max_Sum_Pt2 << std::endl;
*/
      ElementLink<xAOD::VertexContainer>  OrigPvMaxSumPt2Link = DiMuon->auxdata< ElementLink<xAOD::VertexContainer>  >("OrigPvMaxSumPt2Link");
      ElementLink<xAOD::VertexContainer>  PvMaxSumPt2Link = DiMuon->auxdata< ElementLink<xAOD::VertexContainer>  >("PvMaxSumPt2Link");
/*      ElementLink<xAOD::VertexContainer>  OrigPvMinA0Link = DiMuon->auxdata< ElementLink<xAOD::VertexContainer>  >("OrigPvMinA0Link");
      ElementLink<xAOD::VertexContainer>  PvMinA0Link = DiMuon->auxdata< ElementLink<xAOD::VertexContainer>  >("PvMinA0Link");
      ElementLink<xAOD::VertexContainer>  OrigPvMinZ0Link = DiMuon->auxdata< ElementLink<xAOD::VertexContainer>  >("OrigPvMinZ0Link");
      ElementLink<xAOD::VertexContainer>  PvMinZ0Link = DiMuon->auxdata< ElementLink<xAOD::VertexContainer>  >("PvMinZ0Link");
*/
      const xAOD::Vertex* vx_orig = NULL;
      const xAOD::Vertex* vx = NULL;
      if(OrigPvMaxSumPt2Link.isValid()) vx_orig = *OrigPvMaxSumPt2Link;
      if(PvMaxSumPt2Link.isValid()) vx = *PvMaxSumPt2Link;
      if(OrigPvMaxSumPt2Link.isValid()) miT_pvx_orig_z_MaxSumPt2.push_back(vx_orig->z());
      if(PvMaxSumPt2Link.isValid()) miT_pvx_ref_z_MaxSumPt2.push_back(vx->z());
 
      //===============================================================
      // Truth Matching
      //===============================================================
      const xAOD::TruthParticle* TruthMatch_Muon0 = NULL;
      const xAOD::TruthParticle* TruthMatch_Muon1 = NULL;


      // ALSO NEED TO ADD MATCH PROBABILITY TO NTUPLE
      if(m_config->isMC()) {
        if(Muon0_ptr->isAvailable<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink")) {
          ElementLink<xAOD::TruthParticleContainer> link = Muon0_ptr->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
          if(link.isValid()) TruthMatch_Muon0 = *link;
          }
 
          if(Muon1_ptr->isAvailable<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink")) {
            ElementLink<xAOD::TruthParticleContainer> link = Muon1_ptr->auxdata<ElementLink<xAOD::TruthParticleContainer> >("truthParticleLink");
            if(link.isValid()) TruthMatch_Muon1 = *link;
          }
        }

      //===============================================================
      //===============================================================
      // Selected Candidates
      // Beyond here, we keep everything...
      //===============================================================

      // Fill Mini Tree
      if(m_config->isMC()) {
        if(TruthMatch_Muon0 != NULL && TruthMatch_Muon1 != NULL) {
          miT_TruthMatch.push_back(1);
        }else{
          miT_TruthMatch.push_back(0);
        }
      }else{
        miT_TruthMatch.push_back(0);
      }
 
      miT_DiMuon_Mass.push_back(DiMuon_Mass);
      miT_DiMuon_Mass_Err.push_back(DiMuon_MassErr);

      miT_DiMuon_Vtxx.push_back(DiMuon->x());
      miT_DiMuon_Vtxy.push_back(DiMuon->y());
      miT_DiMuon_Vtxz.push_back(DiMuon->z()); 

      miT_DiMuon_Pt.push_back(RefDiMuon.Pt());
      miT_DiMuon_Rapidity.push_back(RefDiMuon.Rapidity());
      miT_DiMuon_Eta.push_back(RefDiMuon.Eta());
      miT_DiMuon_Phi.push_back(RefDiMuon.Phi());


      if ( Track0->charge() > 0.0 && Track1->charge() < 0.0) {

        miT_MuPlus_Track_Pt.push_back(Track0->pt());
        miT_MuPlus_Track_Eta.push_back(Track0->eta());
        miT_MuPlus_Track_Phi.push_back(Track0->phi());

        miT_MuPlus_Tight.push_back(mu0_tight);
//        miT_MuPlus_RecoEff.push_back(mu0_recoEff);

        miT_MuMinus_Track_Pt.push_back(Track1->pt());
        miT_MuMinus_Track_Eta.push_back(Track1->eta());
        miT_MuMinus_Track_Phi.push_back(Track1->phi());

        miT_MuMinus_Tight.push_back(mu1_tight);
 //       miT_MuMinus_RecoEff.push_back(mu1_recoEff);
/*
        miT_Match_MuPlus_HLT_mu4.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu4", 0.01));
        miT_Match_MuPlus_HLT_mu4_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu4_bJpsi_Trkloose", 0.01));
        miT_Match_MuPlus_HLT_mu6_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu6_bJpsi_Trkloose", 0.01));
        miT_Match_MuPlus_HLT_mu10_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu10_bJpsi_Trkloose", 0.01));
        miT_Match_MuPlus_HLT_mu18_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu18_bJpsi_Trkloose", 0.01));

        miT_Match_MuMinus_HLT_mu4.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu4", 0.01));
        miT_Match_MuMinus_HLT_mu4_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu4_bJpsi_Trkloose", 0.01));
        miT_Match_MuMinus_HLT_mu6_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu6_bJpsi_Trkloose", 0.01));
        miT_Match_MuMinus_HLT_mu10_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu10_bJpsi_Trkloose", 0.01));
        miT_Match_MuMinus_HLT_mu18_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu18_bJpsi_Trkloose", 0.01));

        miT_Match_MuPlus_HLT_mu20_L1MU15.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu20_L1MU15", 0.01));
        miT_Match_MuMinus_HLT_mu20_L1MU15.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu20_L1MU15", 0.01));
*/
      }
      else if ( Track0->charge() < 0.0 && Track1->charge() > 0.0) {

        miT_MuPlus_Track_Pt.push_back(Track1->pt());
        miT_MuPlus_Track_Eta.push_back(Track1->eta());
        miT_MuPlus_Track_Phi.push_back(Track1->phi());
        miT_MuPlus_Tight.push_back(mu1_tight);
//        miT_MuPlus_RecoEff.push_back(mu1_recoEff);

        miT_MuMinus_Track_Pt.push_back(Track0->pt());
        miT_MuMinus_Track_Eta.push_back(Track0->eta());
        miT_MuMinus_Track_Phi.push_back(Track0->phi());

        miT_MuMinus_Tight.push_back(mu0_tight);
//        miT_MuMinus_RecoEff.push_back(mu0_recoEff);
/*
        miT_Match_MuPlus_HLT_mu4.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu4", 0.01));
        miT_Match_MuPlus_HLT_mu4_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu4_bJpsi_Trkloose", 0.01));
        miT_Match_MuPlus_HLT_mu6_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu6_bJpsi_Trkloose", 0.01));
        miT_Match_MuPlus_HLT_mu10_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu10_bJpsi_Trkloose", 0.01));
        miT_Match_MuPlus_HLT_mu18_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu18_bJpsi_Trkloose", 0.01));

        miT_Match_MuMinus_HLT_mu4.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu4", 0.01));
        miT_Match_MuMinus_HLT_mu4_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu4_bJpsi_Trkloose", 0.01));
        miT_Match_MuMinus_HLT_mu6_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu6_bJpsi_Trkloose", 0.01));
        miT_Match_MuMinus_HLT_mu10_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu10_bJpsi_Trkloose", 0.01));
        miT_Match_MuMinus_HLT_mu18_bJpsi_Trkloose.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu18_bJpsi_Trkloose", 0.01));

        miT_Match_MuPlus_HLT_mu20_L1MU15.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon1, "HLT_mu20_L1MU15", 0.01));
        miT_Match_MuMinus_HLT_mu20_L1MU15.push_back(m_TrigBphysMatchingTool->getMuonMatchingTool()->match(Muon0, "HLT_mu20_L1MU15", 0.01));
*/
      }
      else {

        miT_MuPlus_Track_Pt.push_back(-999);
        miT_MuPlus_Track_Eta.push_back(-999);
        miT_MuPlus_Track_Phi.push_back(-999);

        miT_MuMinus_Track_Pt.push_back(-999);
        miT_MuMinus_Track_Eta.push_back(-999);
        miT_MuMinus_Track_Phi.push_back(-999);
/*
        miT_Match_MuPlus_HLT_mu4.push_back(-1);
        miT_Match_MuPlus_HLT_mu4_bJpsi_Trkloose.push_back(-1);
        miT_Match_MuPlus_HLT_mu6_bJpsi_Trkloose.push_back(-1);
        miT_Match_MuPlus_HLT_mu10_bJpsi_Trkloose.push_back(-1);
        miT_Match_MuPlus_HLT_mu18_bJpsi_Trkloose.push_back(-1);

        miT_Match_MuMinus_HLT_mu4.push_back(-1);
        miT_Match_MuMinus_HLT_mu4_bJpsi_Trkloose.push_back(-1);
        miT_Match_MuMinus_HLT_mu6_bJpsi_Trkloose.push_back(-1);
        miT_Match_MuMinus_HLT_mu10_bJpsi_Trkloose.push_back(-1);
        miT_Match_MuMinus_HLT_mu18_bJpsi_Trkloose.push_back(-1);

        miT_Match_MuPlus_HLT_mu20_L1MU15.push_back(-1);
        miT_Match_MuMinus_HLT_mu20_L1MU15.push_back(-1);
*/
      }
/*
      BphysMatchResult Match_Pair_HLT_2mu4_bJpsimumu = m_TrigBphysMatchingTool->matchDimuon(*Muon0, *Muon1, "HLT_2mu4_bJpsimumu", 0.01);
      miT_Match_Pair_HLT_2mu4_bJpsimumu.push_back(Match_Pair_HLT_2mu4_bJpsimumu.matched());

      BphysMatchResult Match_Pair_HLT_2mu4_bDimu = m_TrigBphysMatchingTool->matchDimuon(*Muon0, *Muon1, "HLT_2mu4_bDimu", 0.01);
      miT_Match_Pair_HLT_2mu4_bDimu.push_back(Match_Pair_HLT_2mu4_bDimu.matched());

      BphysMatchResult Match_Pair_HLT_2mu4_bDimu_novtx_noos = m_TrigBphysMatchingTool->matchDimuon(*Muon0, *Muon1, "HLT_2mu4_bDimu_novtx_noos", 0.01);
      miT_Match_Pair_HLT_2mu4_bDimu_novtx_noos.push_back(Match_Pair_HLT_2mu4_bDimu_novtx_noos.matched());
*/
      miT_Tau_InvMass_MaxSumPt2.push_back(DiMuon->auxdata<float>("Jpsi_TauInvMassPVMaxSumPt2"));
      miT_Tau_JpsiMass_MaxSumPt2.push_back(DiMuon->auxdata<float>("Jpsi_TauConstMassPVMaxSumPt2"));

      miT_TauErr_InvMass_MaxSumPt2.push_back(DiMuon->auxdata<float>("Jpsi_TauErrInvMassPVMaxSumPt2"));
      miT_TauErr_JpsiMass_MaxSumPt2.push_back(DiMuon->auxdata<float>("Jpsi_TauErrConstMassPVMaxSumPt2"));

      miT_Tau_InvMass_Min_A0.push_back(DiMuon->auxdata<float>("Jpsi_TauInvMassPVMinA0"));
      miT_Tau_JpsiMass_Min_A0.push_back(DiMuon->auxdata<float>("Jpsi_TauConstMassPVMinA0"));

      miT_TauErr_InvMass_Min_A0.push_back(DiMuon->auxdata<float>("Jpsi_TauErrInvMassPVMinA0"));
      miT_TauErr_JpsiMass_Min_A0.push_back(DiMuon->auxdata<float>("Jpsi_TauErrConstMassPVMinA0"));

      miT_Tau_InvMass_Min_Z0.push_back(DiMuon->auxdata<float>("Jpsi_TauInvMassPVMinZ0"));
      miT_Tau_JpsiMass_Min_Z0.push_back(DiMuon->auxdata<float>("Jpsi_TauConstMassPVMinZ0"));

      miT_TauErr_InvMass_Min_Z0.push_back(DiMuon->auxdata<float>("Jpsi_TauErrInvMassPVMinZ0"));
      miT_TauErr_JpsiMass_Min_Z0.push_back(DiMuon->auxdata<float>("Jpsi_TauErrConstMassPVMinZ0"));

      miT_Lxy_Min_A0.push_back(DiMuon->auxdata<float>("LxyMinA0"));
      miT_LxyErr_Min_A0.push_back(DiMuon->auxdata<float>("LxyErrMinA0"));

      miT_Lxy_MaxSumPt2.push_back(DiMuon->auxdata<float>("LxyMaxSumPt2"));
      miT_LxyErr_MaxSumPt2.push_back(DiMuon->auxdata<float>("LxyErrMaxSumPt2"));

      miT_Lxy_Min_Z0.push_back(DiMuon->auxdata<float>("LxyMinZ0"));
      miT_LxyErr_Min_Z0.push_back(DiMuon->auxdata<float>("LxyErrMinZ0"));
      
      miT_Z0_Min_A0.push_back(DiMuon->auxdata<float>("Z0MinA0"));
      miT_Z0Err_Min_A0.push_back(DiMuon->auxdata<float>("Z0ErrMinA0"));

      miT_Z0_Min_Z0.push_back(DiMuon->auxdata<float>("Z0MinZ0"));
      miT_Z0Err_Min_Z0.push_back(DiMuon->auxdata<float>("Z0ErrMinZ0"));

      miT_Z0_MaxSumPt2.push_back(DiMuon->auxdata<float>("Z0MaxSumPt2"));
      miT_Z0Err_MaxSumPt2.push_back(DiMuon->auxdata<float>("Z0ErrMaxSumPt2"));

      miT_A0_Min_A0.push_back(DiMuon->auxdata<float>("A0MinA0"));
      miT_A0Err_Min_A0.push_back(DiMuon->auxdata<float>("A0ErrMinA0"));

      miT_A0_Min_Z0.push_back(DiMuon->auxdata<float>("A0MinZ0"));
      miT_A0Err_Min_Z0.push_back(DiMuon->auxdata<float>("A0ErrMinZ0"));

      miT_A0_MaxSumPt2.push_back(DiMuon->auxdata<float>("A0MaxSumPt2"));
      miT_A0Err_MaxSumPt2.push_back(DiMuon->auxdata<float>("A0ErrMaxSumPt2"));

      miT_A0xy_Min_A0.push_back(DiMuon->auxdata<float>("A0xyMinA0"));
      miT_A0xyErr_Min_A0.push_back(DiMuon->auxdata<float>("A0xyErrMinA0"));

      miT_A0xy_Min_Z0.push_back(DiMuon->auxdata<float>("A0xyMinZ0"));
      miT_A0xyErr_Min_Z0.push_back(DiMuon->auxdata<float>("A0xyErrMinZ0"));

      miT_A0xy_MaxSumPt2.push_back(DiMuon->auxdata<float>("A0xyMaxSumPt2"));
      miT_A0xyErr_MaxSumPt2.push_back(DiMuon->auxdata<float>("A0xyErrMaxSumPt2"));

      m_Muon0_RefTrack_Pt.push_back(   Muon0_RefTrack.Pt());
      m_Muon0_RefTrack_Eta.push_back(  Muon0_RefTrack.Eta());
      m_Muon0_RefTrack_Phi.push_back(	 Muon0_RefTrack.Phi());
      m_Muon0_RefTrack_Theta.push_back(Muon0_RefTrack.Theta());
      m_Muon0_Track_Pt.push_back(   Track0->pt());
      m_Muon0_Track_Eta.push_back(  Track0->eta());
      m_Muon0_Track_Phi.push_back(  Track0->phi());
      m_Muon0_Track_Theta.push_back(Track0->theta());
      m_Muon0_Track_d0.push_back(   Track0->d0());
      m_Muon0_Track_z0.push_back(   Track0->z0());
      m_Muon0_CBTrack_Pt.push_back(   Muon0_ptr->pt());
      m_Muon0_CBTrack_Eta.push_back(  Muon0_ptr->eta());
      m_Muon0_CBTrack_Phi.push_back(  Muon0_ptr->phi());
      m_Muon0_Charge.push_back(	Muon0_ptr->charge());
      m_Muon0_Quality.push_back(	muon0_qual);
      
      
      m_Muon1_RefTrack_Pt.push_back(   Muon1_RefTrack.Pt());
      m_Muon1_RefTrack_Eta.push_back(  Muon1_RefTrack.Eta());
      m_Muon1_RefTrack_Phi.push_back(  Muon1_RefTrack.Phi());
      m_Muon1_RefTrack_Theta.push_back(Muon1_RefTrack.Theta());
      m_Muon1_Track_Pt.push_back(   Track1->pt());
      m_Muon1_Track_Eta.push_back(  Track1->eta());
      m_Muon1_Track_Phi.push_back(  Track1->phi());
      m_Muon1_Track_Theta.push_back(Track1->theta());
      m_Muon1_Track_d0.push_back(   Track1->d0());
      m_Muon1_Track_z0.push_back(   Track1->z0());
      m_Muon1_CBTrack_Pt.push_back(   Muon1_ptr->pt());
      m_Muon1_CBTrack_Eta.push_back(  Muon1_ptr->eta());
      m_Muon1_CBTrack_Phi.push_back(  Muon1_ptr->phi());
      m_Muon1_Charge.push_back(       Muon1_ptr->charge());
      m_Muon1_Quality.push_back(	muon1_qual);


	  }
    return;
  }

  void TtbarJpsiEventSaver::AddJpsiTruth(const top::Event& event){
    //Clear vectors
    miT_Truth_Jpsi_Pt.clear();
    miT_Truth_Jpsi_Eta.clear();
    miT_Truth_Jpsi_Phi.clear();
    miT_Truth_Jpsi_Mass.clear();

    miT_Truth_MuPlus_Pt.clear();
    miT_Truth_MuPlus_Eta.clear();
    miT_Truth_MuPlus_Phi.clear();

    miT_Truth_MuMinus_Pt.clear();
    miT_Truth_MuMinus_Eta.clear();
    miT_Truth_MuMinus_Phi.clear();

    miT_Truth_PV_Z.clear();
    //---------------------------
    // Truth Vertex
    //---------------------------
    const xAOD::TruthVertexContainer* truthVertices = 0;
    top::check( evtStore()->retrieve(truthVertices,"TruthVertices"),"Failed to retrieve TruthVertices. Exiting.");
 
    // First in collection is PV
    const xAOD::TruthVertex* PriVertex = (*truthVertices)[0];
 
    //---------------------------
    // Truth Particles
    //---------------------------
    const xAOD::TruthParticleContainer* truthParticles = 0;
    top::check( evtStore()->retrieve(truthParticles,"TruthParticles"),"Failed to retrieve TruthParticles. Exiting.");
 
    // Add the Onia of interest
    std::vector<int> OniaPDG;
    OniaPDG.push_back(443);
    OniaPDG.push_back(100443);
   
    // Loop over truth particles
    for(unsigned int t = 0; t < truthParticles->size(); ++t) {
 
      const xAOD::TruthParticle* particle = (*truthParticles)[t];
 
      bool FoundOnia = false;
      for(unsigned int p = 0; p < OniaPDG.size(); ++p) {
        if(particle->pdgId() == OniaPDG[p]) FoundOnia = true;
      }
 
      if(!FoundOnia) continue;
 
      // Check particle has a parent and 2 decay products (muons)
      if(particle->nParents() < 1) continue;
 
      // Sometimes there is a photon radiated,
      // so can have more than 2 children
      if(particle->nChildren() < 2) continue;
 
 
      // The first two children are the status=1 final state muons...
      const xAOD::TruthParticle* muon1  = particle->child (0);
      const xAOD::TruthParticle* muon2  = particle->child (1);
 
      // Check it decayed to muons
      if(muon1->absPdgId() != 13) continue;
      if(muon2->absPdgId() != 13) continue;
 
      // Check particle decayed and it has a production vertex
      if(!particle->hasDecayVtx()) continue;
      if(!particle->hasProdVtx()) continue;
 
      //const xAOD::TruthVertex* prodVertex   = particle->prodVtx();
      //const xAOD::TruthVertex* decayVertex  = particle->decayVtx();
 
      // Check both muons are final state
      if(muon1->status() != 1) continue;
      if(muon2->status() != 1) continue;
 
      miT_Truth_Jpsi_Pt.push_back( particle->p4().Pt() );
      miT_Truth_Jpsi_Eta.push_back( particle->p4().Eta() );
      miT_Truth_Jpsi_Phi.push_back( particle->p4().Phi() );
      miT_Truth_Jpsi_Mass.push_back( particle->p4().M() );
 
      if(muon1->pdgId() == 13 && muon2->pdgId() == -13) {
           
        miT_Truth_MuPlus_Pt.push_back( muon2->p4().Pt() );
        miT_Truth_MuPlus_Eta.push_back( muon2->p4().Eta() );
        miT_Truth_MuPlus_Phi.push_back( muon2->p4().Phi() );
 
        miT_Truth_MuMinus_Pt.push_back( muon1->p4().Pt() );
        miT_Truth_MuMinus_Eta.push_back( muon1->p4().Eta() );
        miT_Truth_MuMinus_Phi.push_back( muon1->p4().Phi() );
 
      }
      else
        if(muon1->pdgId() == -13 && muon2->pdgId() == 13) {
 
          miT_Truth_MuPlus_Pt.push_back( muon1->p4().Pt() );
          miT_Truth_MuPlus_Eta.push_back( muon1->p4().Eta() );
          miT_Truth_MuPlus_Phi.push_back( muon1->p4().Phi() );
 
          miT_Truth_MuMinus_Pt.push_back( muon2->p4().Pt() );
          miT_Truth_MuMinus_Eta.push_back( muon2->p4().Eta() );
          miT_Truth_MuMinus_Phi.push_back( muon2->p4().Phi() );
 
        } 
        else {
 
          miT_Truth_MuPlus_Pt.push_back( -999 );
          miT_Truth_MuPlus_Eta.push_back( -999 );
          miT_Truth_MuPlus_Phi.push_back( -999 );
 
          miT_Truth_MuMinus_Pt.push_back( -999 );
          miT_Truth_MuMinus_Eta.push_back( -999 );
          miT_Truth_MuMinus_Phi.push_back( -999 );
 
        }
 
        miT_Truth_PV_Z.push_back( PriVertex->z() );
 
    } // Loop over truth particles

    return;
  }

  // =======================================================
  void TtbarJpsiEventSaver::AddTriggerPrescales() 
  {    
    //
    // for trigger prescales
    //

    if(m_config->isMC()) return;

    for( auto& trig_PS : m_triggerPrescales ) {
      auto cg = m_trigDecisionTool->getChainGroup(trig_PS.first);
      trig_PS.second = cg->getPrescale();
    }
    return;
  }

  // ==================================================================
  int TtbarJpsiEventSaver::getBranchStatus(top::TreeManager const * treeManager, std::string const & variableName) 
  {    
    //we can use the treeManager to remove these branches only for some of the TTrees
    // e.g. add a condition like if (m_config->systematicName(treeManager->name() != "nominal")
    if (variableName == "jet_ip3dsv1" || variableName == "jet_mv2c00" || variableName == "jet_mv2c20") return 0;// we only need mv2c10
    if (variableName.find("weight_indiv_")!=std::string::npos) return 0;// we don't care about the individual SFs
    if (variableName.find("tau_")!=std::string::npos || variableName.find("weight_tau") != std::string::npos) return 0;// we don't care about the tau SFs
    return -1;
  }

  StatusCode TtbarJpsiEventSaver::passMuonLoose (const xAOD::Muon* muon, bool& isPassed) const {
    isPassed =  m_muonSelectionTool_loose->accept(*muon);
    return StatusCode::SUCCESS;
  }
  StatusCode TtbarJpsiEventSaver::passMuonMedium(const xAOD::Muon* muon, bool& isPassed) const {
    isPassed =  m_muonSelectionTool_medium->accept(*muon);
    return StatusCode::SUCCESS;
  }
  StatusCode TtbarJpsiEventSaver::passMuonTight (const xAOD::Muon* muon, bool& isPassed) const {
    isPassed =  m_muonSelectionTool_tight->accept(*muon);
    return StatusCode::SUCCESS;
  }
  StatusCode TtbarJpsiEventSaver::passMuonLowPt (const xAOD::Muon* muon, bool& isPassed) const {
    isPassed =  m_muonSelectionTool_lowpt->accept(*muon);
    return StatusCode::SUCCESS;
  } 


  std::unique_ptr<xAOD::Muon> TtbarJpsiEventSaver::makeCalibMuon(const xAOD::Muon* origMu) const {
    xAOD::Muon* newMuon(nullptr);
    
    auto corrCode = m_muonCalibSmearTool->correctedCopy(*origMu,newMuon);
    if (corrCode != CP::CorrectionCode::Ok) {
        ATH_MSG_WARNING("Muon smearing gave error: " << corrCode);
    }
    std::unique_ptr<xAOD::Muon> nm(newMuon);
    return nm;
  }


  const CP::IMuonEfficiencyScaleFactors* TtbarJpsiEventSaver::muonEffSFTightTool() const {
    return m_muonEffSFTightTool.get();
  }
  const CP::IMuonEfficiencyScaleFactors* TtbarJpsiEventSaver::muonEffSFMediumTool() const {
    return m_muonEffSFMediumTool.get();
  }
  const CP::IMuonEfficiencyScaleFactors* TtbarJpsiEventSaver::muonEffSFLooseTool() const {
    return m_muonEffSFLooseTool.get();
  }
  const CP::IMuonEfficiencyScaleFactors* TtbarJpsiEventSaver::muonEffSFLowPtTool() const {
    if (m_muonEffSFLowPtTool.isInitialized()) return m_muonEffSFLowPtTool.get();
    else {
        ATH_MSG_WARNING("m_muonEffSFLowPtTool not initialized; returning nullptr; likely your code will now crash");
        return nullptr;
    }
  }

  // from Michael

  double TtbarJpsiEventSaver::phiMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const {
    
    TLorentzVector MuPlus;
    TLorentzVector MuMinus;
    
    if(Mu1_q > 0){
        MuPlus = Mu1;
        MuMinus = Mu2;
    }
    else{
        MuPlus = Mu2;
        MuMinus = Mu1;
    }
    
    
    TLorentzVector MuMu = MuPlus + MuMinus;
    TVector3 V0 = MuMu.Vect();
    MuPlus.RotateZ(-V0.Phi());
    MuPlus.RotateY(-V0.Theta());
    return atan2(MuPlus.Y(),MuPlus.X());
  }


  double TtbarJpsiEventSaver::cosMethod(const TLorentzVector & Mu1, const TLorentzVector Mu2, int Mu1_q) const {
    TLorentzVector MuPlus;
    TLorentzVector MuMinus;
    
    if(Mu1_q > 0){
        MuPlus = Mu1;
        MuMinus = Mu2;
    }
    else{
        MuPlus = Mu2;
        MuMinus = Mu1;
    }
    
    
    TLorentzVector MuMu = MuPlus + MuMinus;
    
    double MuMu_mass = MuMu.M();
    double MuPlus_mass = MuPlus.M();
    double MuMinus_mass = MuMinus.M();
    double P_MuMu = MuMu.Rho();
    
    double pssq = ((MuMu_mass*MuMu_mass) - (MuPlus_mass+MuMinus_mass)*(MuPlus_mass+MuMinus_mass)) *  ((MuMu_mass*MuMu_mass) - (MuPlus_mass-MuMinus_mass)*(MuPlus_mass-MuMinus_mass));
    
    double ps = (pssq>0.) ? sqrt(pssq) : 0.;
    
    ps/=2.*MuMu_mass;
    
    double pp = MuMu.Px()*MuPlus.Px() + MuMu.Py()*MuPlus.Py() + MuMu.Pz()*MuPlus.Pz();
    
    double cosTheta = (MuMu.Energy()*pp - P_MuMu*P_MuMu*MuPlus.Energy())/(MuMu_mass*ps*P_MuMu);
    
    
    return cosTheta;
    
  }


  bool TtbarJpsiEventSaver::VatoHelicity(const TLorentzVector & mupl,
                             const TLorentzVector & mumi,
                             TVector3 & HelicityAxis,
                             TVector3 & xAxis,
                             TVector3 & yAxis,
                             double & cosTheta_mupl,
                             double & cosTheta_mumi,
                             double & phi_mupl,
                             double & phi_mumi) const {
    
    TLorentzVector dimu  = mupl + mumi;
    
    double ProtonMass = 938.272; // MeV
    double BeamEnergy = 6500000.; // MeV
    
    // 1)
    TLorentzVector p1, p2;
    p1.SetPxPyPzE(0.,0.,BeamEnergy,
                  TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass));
    p2.SetPxPyPzE(0.,0.,-1*BeamEnergy,
                  TMath::Sqrt(BeamEnergy*BeamEnergy+ProtonMass*ProtonMass));
    
    // 2)
    TLorentzVector dimu_hf, mupl_hf, mumi_hf, p1_hf, p2_hf;
    dimu_hf   = dimu ;
    mupl_hf   = mupl ;
    mumi_hf   = mumi ;
    p1_hf     = p1;
    p2_hf     = p2;
    
    dimu_hf   .Boost(-dimu.BoostVector());
    mupl_hf   .Boost(-dimu.BoostVector());
    mumi_hf   .Boost(-dimu.BoostVector());
    p1_hf     .Boost(-dimu.BoostVector());
    p2_hf     .Boost(-dimu.BoostVector());
    
    // 3)
    HelicityAxis = dimu.Vect().Unit();
    yAxis        = (p1_hf.Vect().Unit()).Cross((p2_hf.Vect().Unit()));
    yAxis        = yAxis.Unit();
    xAxis        = yAxis.Cross(HelicityAxis);
    xAxis        = xAxis.Unit();
    
    // 4)
    phi_mupl      = atan2((mupl_hf.Vect()*yAxis),(mupl_hf.Vect()*xAxis));
    cosTheta_mupl = mupl_hf.Vect().Dot(HelicityAxis) / (mupl_hf.Vect().Mag());
    
    phi_mumi      = atan2((mumi_hf.Vect()*yAxis),(mumi_hf.Vect()*xAxis));
    cosTheta_mumi = mumi_hf.Vect().Dot(HelicityAxis) / (mumi_hf.Vect().Mag());
    
    
    //    ATH_MSG_INFO("MuPl:");
    //    mupl.Print();
    //    mupl_hf.Print();
    //    ATH_MSG_INFO("MuMi:");
    //    mumi.Print();
    //    mumi_hf.Print();
    //    ATH_MSG_INFO("Dimu:");
    //    dimu.Print();
    //    dimu_hf.Print();
    //    ATH_MSG_INFO("p1:");
    //    p1.Print();
    //    p1_hf.Print();
    //    ATH_MSG_INFO("p2:");
    //    p2.Print();
    //    p2_hf.Print();
    //    ATH_MSG_INFO("HelicityAxis:");
    //    HelicityAxis.Print();
    //    ATH_MSG_INFO("xAxis:");
    //    xAxis.Print();
    //    ATH_MSG_INFO("yAxis:");
    //    yAxis.Print();
    
    ATH_MSG_VERBOSE("cosTheta / phi mupl : " << cosTheta_mupl << " " << phi_mupl );
    ATH_MSG_VERBOSE("cosTheta / phi mumi : " << cosTheta_mumi << " " << phi_mumi );

    
    
    if (fabs(cosTheta_mupl + cosTheta_mumi) > 0.001) {
        ATH_MSG_WARNING("VatoHelicty: cosTheta sum, beyond tolerance: " << cosTheta_mupl << " " << cosTheta_mumi);
    }
    if (fabs(sin(phi_mupl - phi_mumi)) > 0.001) {
        ATH_MSG_WARNING("VatoHelicty: phi difference, beyond tolerance: " << phi_mupl << " " << phi_mumi);
    }
    
    return true;
  }



  bool TtbarJpsiEventSaver::VatoHelicity(const TLorentzVector & mupl, const TLorentzVector & mumi,
                  double & cosTheta_mupl,double & phi_mupl) const {
    
    
    TVector3 HelicityAxis;
    TVector3 xAxis;
    TVector3 yAxis;
    double cosTheta_mumi;
    double phi_mumi;

    return TtbarJpsiEventSaver::VatoHelicity(mupl,
                                    mumi,
                                    HelicityAxis,
                                    xAxis,
                                    yAxis,
                                    cosTheta_mupl,
                                    cosTheta_mumi,
                                    phi_mupl,
                                    phi_mumi);
  }

 

  const xAOD::EventInfo* TtbarJpsiEventSaver::eventInfo_f() const {
    const xAOD::EventInfo* evtInfo = 0;
    if (evtStore()->retrieve( evtInfo, "EventInfo" ).isSuccess()) return evtInfo;
    else throw;
  }


}
