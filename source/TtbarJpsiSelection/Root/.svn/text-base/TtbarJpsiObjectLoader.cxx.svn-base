///////////////////////////////////////////////////////////////////////////
// AUTHOR     Frédéric Derue (LPNHE Paris, derue@lpnhe.in2p3.fr) 
// DEVELOPPER 
//            Frédéric Derue (LPNHE Paris)

// PURPOSE    Object loader for ttbar+Jpsi selection
//            code evolved from https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/PhysAnalysis/Run2/HowtoExtendAnalysisTop/trunk/HowtoExtendAnalysisTop
//            and https://svnweb.cern.ch/trac/atlasphys-top/browser/Physics/Top/PhysAnalysis/Run2/TopFakesUtils/trunk/Root/TopFakesObjectLoader.cxx
//
// UPDATE     18/07/2016 creation of the package (from existing private code)
//            20/07/2016 use standard muon selection
//
/////////////////////////////////////////////////////////////////////////

#include "TopEvent/EventTools.h"
#include "TopConfiguration/TopConfig.h"

#include "TopObjectSelectionTools/TopObjectSelection.h"
#include "TopObjectSelectionTools/ElectronLikelihoodMC15.h"
#include "TopObjectSelectionTools/ElectronCutBasedMC15.h"
#include "TopObjectSelectionTools/IsolationTools.h"
#include "TopObjectSelectionTools/MuonMC15.h"
#include "TopObjectSelectionTools/TauMC15.h"
#include "TopObjectSelectionTools/JetMC15.h"
#include "TopObjectSelectionTools/OverlapRemovalASG.h"

///-- The custom object selections we have defined in this package --///
#include "TtbarJpsiSelection/TtbarJpsiObjectLoader.h"
#include "TtbarJpsiSelection/TtbarJpsiMuon.h"
#include "TtbarJpsiSelection/TtbarJpsiJet.h"

namespace top {

  top::TopObjectSelection* TtbarJpsiObjectLoader::init(std::shared_ptr<top::TopConfig> topConfig)
  {
    //
    // initialisation
    //
    top::TopObjectSelection* objectSelection = new top::TopObjectSelection(topConfig->objectSelectionName());
    top::check(objectSelection->setProperty( "config" , topConfig ) , "Failed to setProperty for top::TopObjectSelection" );
    top::check(objectSelection->initialize() , "Failed to initialize top::TopObjectSelection" );
    
    // Debug messages?
    // objectSelection->msg().setLevel(MSG::DEBUG);
    
    ///-- Electrons --///
    if (topConfig->useElectrons()) {
      if (topConfig->electronID().find("LH") == std::string::npos && topConfig->electronIDLoose().find("LH") == std::string::npos) {
	//both the tight and loose user settings do not contain LH -> cut based
        objectSelection->electronSelection(new top::ElectronCutBasedMC15(topConfig->electronPtcut(),
                                                                         topConfig->electronVetoLArCrack(),
                                                                         topConfig->electronID(),
                                                                         topConfig->electronIDLoose(),
                                                                         new top::StandardIsolation(
												    topConfig->electronIsolation() ,
												    topConfig->electronIsolationLoose() )
                                                                         ) );
	
      } else if (topConfig->electronID().find("LH") != std::string::npos && topConfig->electronIDLoose().find("LH") != std::string::npos) {
          //user wants likelihood electrons
	objectSelection->electronSelection(new top::ElectronLikelihoodMC15(topConfig->isPrimaryxAOD(),
									   topConfig->electronPtcut(),
									   topConfig->electronVetoLArCrack(),
									   topConfig->electronID(),
									   topConfig->electronIDLoose(),
									   new top::StandardIsolation(
												      topConfig->electronIsolation() ,
												      topConfig->electronIsolationLoose() )
									   ) );
      } else {
	std::cout << "\nHo hum\n";
	std::cout << "Not sure it makes sense to use a mix of LH and cut-based electrons for the tight/loose definitions\n";
	std::cout << "Tight electron definition is " << topConfig->electronID() << "\n";
	std::cout << "Loose electron definition is " << topConfig->electronIDLoose() << "\n";
	std::cout << "If it does make sense, feel free to fix this\n";
	exit(1);
      }
    }
       
    ///-- Muons --///
    if (topConfig->useMuons()) {
      // using standard selection
      //std::cout << " performs standard selection " << std::endl;
      objectSelection -> muonSelection(new top::MuonMC15(topConfig->muonPtcut(), new top::StandardIsolation(
													    topConfig->muonIsolation() , 
													    topConfig->muonIsolationLoose())
							 ) );
      // using custom selection
      //std::cout << " performs custom selection " << std::endl;
      //objectSelection -> muonSelection(new top::TtbarJpsiMuon(topConfig->muonPtcut(), new top::StandardIsolation())); 
    }
    
    ///-- Jets --///
    if (topConfig->useJets()) {
      // using standard selection
      objectSelection -> jetSelection(new top::JetMC15(topConfig->jetPtcut(),
						       topConfig->jetEtacut(),
						       topConfig->jetJVTcut()
						       ) );
      // using cutom selection
      //double ptMax(100000.);
      //objectSelection -> jetSelection(new top::TtbarJpsiJet(topConfig->jetPtcut(), ptMax ,topConfig->jetEtacut(), topConfig->jetJVTcut())); 
    }
    
    ///-- Overlap removal --///
    objectSelection->overlapRemovalPostSelection(new top::OverlapRemovalASG());
    return objectSelection;
  }
}
