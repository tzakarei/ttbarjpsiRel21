#!/usr/bin/env python
import TopExamples.grid
import DerivationTags
import MC15
import Data

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.settingsFile  = 'TtbarJpsiCuts.txt'

config.gridUsername  = 'tzakarei'
config.suffix        = '22-04-18_2015DATA_corr_2try'
config.excludedSites = ''
config.noSubmit      = False
config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCALGROUPDISK'
config.maxNFilesPerJob = '5'


###MC Simulation - look in MC15_25ns_TOPQ1.py
###Using list of TOPQ1 25ns MC samples, consistent mixture of p-tags
###Edit these lines if you don't want to run everything!
#names, derivation, ptag = DerivationTags.InteractiveSubmission()

names = [
  'Data16_TOPQ5',
]

samples = TopExamples.grid.Samples(names)
TopExamples.grid.submit(config, samples)

