# CMake generated Testfile for 
# Source directory: /afs/cern.ch/work/t/tzakarei/AthenaRel21/source
# Build directory: /afs/cern.ch/work/t/tzakarei/AthenaRel21/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("TopEventReconstructionTools")
subdirs("TtbarJpsiSelection")
